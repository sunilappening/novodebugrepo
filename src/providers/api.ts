import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers,URLSearchParams,ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Notifications } from '../models/notifications';
import { Client } from '../models/client';
import { Schedule } from '../models/schedule';
import { Assessment } from '../models/assessment';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  //url: string = 'https://novopsych.com/api';
  url: string = 'http://localhost:8100/v1';
  
  credentials: any;

  constructor(public http: Http) {
    
  }

  resetPassword(endpoint: string, body: any) {
    var response =  this.http.post(this.url + '/' + endpoint, JSON.stringify(body))
      .map(res => res.json().data)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
    
  }

  login(endpoint: string, credentials) {
    this.credentials = credentials;
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Authorization', "Basic ".concat(btoa(this.credentials.username+':'+this.credentials.password)));
      let options = new RequestOptions({ headers: headers });
      this.http.get(this.url + '/' + endpoint, options)
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
        
    });
 }

 register(endpoint: string, body: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    let options = new RequestOptions({ headers: headers});
    return this.http.post(this.url + '/' + endpoint, JSON.stringify(body), options);
  }

  addClient(endpoint: string, body: any) {
    var response =  this.http.post(this.url + '/' + endpoint, JSON.stringify(body),this.createAuthorizationHeader());
      //.map(res => res.json().data)
      //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
    
  }

  addNote(endpoint: string, body: any) {
    var response =  this.http.post(this.url + '/' + endpoint + '/' + body.clientId + '/' + 'notes', JSON.stringify(body),this.createAuthorizationHeader());
      // .map(res => <Note>res.json().data.note)
      // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
    
  }

  updateNote(endpoint: string, body: any) {
    var response =  this.http.put(this.url + '/' + endpoint + '/' + body.id, JSON.stringify(body),this.createAuthorizationHeader());
      //.map(res => res.json().data)
      //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
    
  }

  getClients(endpoint: string): Observable<Client[]> {
    var response = this.http.get(this.url + '/' + endpoint, this.createAuthorizationHeader())
      .map(res => <Client[]>res.json().data.clients)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  getClientNotesAndResponses(endpoint: string, clientId: string): Observable<Client> {
    var response = this.http.get(this.url + '/' + endpoint + '/' + clientId, this.createAuthorizationHeader())
      .map(res => <Client>res.json().data.client)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  getNews(endpoint: string, body): Observable<Notifications> {
    var response = this.http.get(this.url + '/' + endpoint, this.createAuthorizationHeader())
      .map(res => <Notifications>res.json().data)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  getAssessments(endpoint: string, version: string): Observable<Assessment[]> {
    let assessmentVersion = new URLSearchParams();
    assessmentVersion.set('version', version);
    let options = this.createAuthorizationHeader();
    options.search = assessmentVersion
    var response = this.http.get(this.url + '/' + endpoint + '/', options)
      .map(res => <Assessment[]>res.json().data.assessments)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  scheduleAssessments(endpoint: string, body: any){
    var response =  this.http.post(this.url + '/' + endpoint, JSON.stringify(body),this.createAuthorizationHeader())
    .map(res => res.json().data)
    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  return response;
  }

  getScheduledAssessments(endpoint: string, scheduleId: string): Observable<Schedule> {
    var response = this.http.get(this.url + '/' + endpoint + '/' + scheduleId, this.createAuthorizationHeader())
      .map(res => <Schedule>res.json().data.schedule)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  deleteSchedule(endpoint: string, scheduleId: string){
    return this.http.delete(this.url + '/' + endpoint + '/' + scheduleId, this.createAuthorizationHeader());
  }

  private createAuthorizationHeader() {
    let userId = localStorage.getItem('userId');
    let password = localStorage.getItem('password');
    if (userId && password) {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Authorization', "Basic ".concat(btoa(userId+':'+password)));
      return new RequestOptions({ headers: headers });
  }
     
}

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  getReportPDF (link:string){
    let userId = localStorage.getItem('userId');
    let password = localStorage.getItem('password');
    return this.http.get(link,
        {
            headers: new Headers({
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With",
                "Authorization": "Basic ".concat(btoa(userId+':'+password))
            }),
            responseType:ResponseContentType.ArrayBuffer // YOU NEED THAT
        }
    );

}
  

}


