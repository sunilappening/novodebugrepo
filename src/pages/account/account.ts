import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  text: string;
  isPlanSelected: boolean;
  isAccountSelected: boolean;
  isFeedbackSelected: boolean;
  isHelpSelected: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.text='plan';
    this.isPlanSelected = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  planSelected(item){
    this.text = item;
    this.isPlanSelected = true;
    this.isAccountSelected = false;
    this.isFeedbackSelected = false;
    this.isHelpSelected = false;
  }

  accountSelected(item){
    this.text = item;
    this.isAccountSelected = true;
    this.isFeedbackSelected = false;
    this.isHelpSelected = false;
    this.isPlanSelected = false;
  }

  feedbackSelected(item){
    this.text = item;
    this.isFeedbackSelected = true;
    this.isPlanSelected = false;
    this.isHelpSelected = false;
    this.isAccountSelected = false;
  }

  helpSelected(item){
    this.text = item;
    this.isHelpSelected = true;
    this.isFeedbackSelected = false;
    this.isPlanSelected = false;
    this.isAccountSelected = false;
  }

}
