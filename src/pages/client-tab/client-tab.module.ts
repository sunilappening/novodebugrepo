import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTabPage } from './client-tab';

@NgModule({
  declarations: [
    ClientTabPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientTabPage),
  ],
})
export class ClientTabPageModule {}
