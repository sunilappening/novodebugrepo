import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the AdministerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-administer',
  templateUrl: 'administer.html',
})
export class AdministerPage {
  clientName: string = this.navParams.get('clientName');
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdministerPage');
    console.log('value of clientName passed' + this.clientName);
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  administer(){
    this.getQuestionsForAssessment();
  }

  getQuestionsForAssessment(){

  }

}
