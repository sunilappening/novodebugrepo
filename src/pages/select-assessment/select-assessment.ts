import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AdministerPage } from '../administer/administer';
//import { TagsInputModule } from 'ionic2-tags-input';
import { Assessment } from '../../models/assessment';
import { Items } from '../../providers/providers';

/**
 * Generated class for the SelectAssessmentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * the tags module is from https://github.com/HsuanXyz/ionic-tags-input
 */


@IonicPage()
@Component({
  selector: 'page-select-assessment',
  templateUrl: 'select-assessment.html',
})
export class SelectAssessmentPage {
  clientId: string = this.navParams.get('clientId');
  clientName: string = this.navParams.get('clientName');
  assessments: Assessment[];
  assessmentsTag: Assessment[] = [];
  assessmentNameTag = [];
  assessment: Assessment;
  selected = [];

  selectedAssessments: { short_name: string, extended_name: string} = {
    short_name: 'First Name',
    extended_name: 'Last Name'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public items: Items) {
    
  }

  ionViewDidEnter() {
    console.log('value of clientId passed' + this.clientId);
    this.assessments = this.items.getAssessments();
    console.log(this.assessmentsTag.length);
    console.log(this.assessmentNameTag.length);
    //this.assessment = this.assessments[0];
  }

  cancel() {
    console.log(this.assessmentsTag.length);
    this.assessmentsTag.length = 0
    console.log(this.assessmentsTag.length);
    this.viewCtrl.dismiss();
  }

  assessmentSelected(item){
    var index = this.assessmentsTag.indexOf(item);
    if (index > -1) {
      console.log("deleting item to assessmentsTag array");
      this.assessmentsTag.pop();
      this.assessmentNameTag.pop();
      item.selected = false;
  } else {
    console.log("adding item to assessmentsTag array");
    //little hack to display the tag name
    this.assessmentsTag.push(item);
    this.assessmentNameTag.push(item.short_name);
    item.selected = true;
  }
    
    
  }

  onChange(val){
    this.assessmentsTag.pop();
    this.assessmentNameTag.pop();
  }

  filterAssessments(ev) {
    var val = ev.target.value;
    this.assessments = this.items.getAssessments(val);
    return this.assessments;
    
  }

  administer(item){
    this.navCtrl.push(AdministerPage, {clientName: this.clientName});
  }

}
