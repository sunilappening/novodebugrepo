import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectAssessmentPage } from './select-assessment';

@NgModule({
  declarations: [
    SelectAssessmentPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectAssessmentPage),
  ],
})
export class SelectAssessmentPageModule {}
