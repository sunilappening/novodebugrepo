import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Items } from '../../providers/providers';

/**
 * Generated class for the ReportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  link: string = this.navParams.get('url');
  reportUrl: any;
  
  constructor(public sanitizer: DomSanitizer,public navCtrl: NavController, public navParams: NavParams, 
    public modalCtrl: ModalController,public viewCtrl: ViewController,public items: Items) {

      this.items.getReport(this.link).subscribe(
            (data:any) => { 
            var file3 = new Blob([data._body], {type: 'application/pdf'});
            this.reportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(file3));
        },
        error => {
            console.log(error);
        }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
}

}
