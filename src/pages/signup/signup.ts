import { Component } from '@angular/core';
import { NavController, ToastController , ViewController} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HomePage } from '../../pages/pages';
import { User } from '../../providers/user';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { firstname: string, lastname: string, email: string, profession: string, password: string, confirm_password: string, title: string } = {
    firstname: '',
    lastname: '',
    email: '',
    profession: '1',
    password: '',
    confirm_password: '',
    title: '1'
  };

  submitClient: boolean = false;
  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,public user: User,public toastCtrl: ToastController,
              public translateService: TranslateService, formBuilder: FormBuilder, public viewCtrl: ViewController) {
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.form = formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
      profession_id: [''],
      password: [''],
      confirm_password: [''],
      title_id: [''],
      country_id: ['83']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
    
  }

  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }

  // doSignup() {
  //   // Attempt to login in through our User service
  //   this.user.signup(this.account).subscribe((resp) => {
  //     this.navCtrl.push(HomePage);
  //   }, (err) => {

  //     this.navCtrl.push(HomePage); // TODO: Remove this when you add your signup endpoint

  //     // Unable to sign up
  //     let toast = this.toastCtrl.create({
  //       message: this.signupErrorString,
  //       duration: 3000,
  //       position: 'top'
  //     });
  //     toast.present();
  //   });
  // }
}