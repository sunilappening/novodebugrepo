import { Component,Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Assessment } from '../../models/assessment';
import { ClientPage } from '../../pages/client/client';

/**
 * Generated class for the AssessmentDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'assessment-details',
  templateUrl: 'assessment-details.html'
})
export class AssessmentDetailsComponent {

  @Input() assessment: Assessment;

  constructor(public modalCtrl: ModalController) {
    console.log('Hello ClientDetailsComponent Component');
  }

  administer() {
    let modal = this.modalCtrl.create(ClientPage,{administer: 'administer'});
    modal.present();
  }

  emailAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'email'});
    modal.present();
  }

  scheduleAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'schedule'});
    modal.present();
  }


}
