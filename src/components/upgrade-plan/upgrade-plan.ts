import { Component, Input } from '@angular/core';

/**
 * Generated class for the UpgradePlanComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'upgrade-plan',
  templateUrl: 'upgrade-plan.html'
})
export class UpgradePlanComponent {

  @Input() text: string;

  // constructor() {
  //   console.log('Hello UpgradePlanComponent Component');
  //   this.text = 'Hello World';
  // }

}
