export class News {
    // constructor(
    //     public id: string, 
    //     public title: string, 
    //     public content:string,
    //     public link:string

    //     ){}

    constructor(private fields: any) {
    // Quick and dirty extend/assign fields to this model
    for (let f in fields) {
      this[f] = fields[f];
    }
  }
}

