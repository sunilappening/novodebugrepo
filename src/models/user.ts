export class UserData {

    id: string;
    title_id: string;
    first_name: string;
    last_name: string;
    profession_id: string;
    email: string;
    max_clients: string;


    constructor() {
    }

}