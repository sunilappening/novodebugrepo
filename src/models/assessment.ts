export class Assessment {
   id: string;
   version: string;
   extended_name: string;
   short_name: string;
   description: string;
   validity: string;
   interpretation: string;
   developer: string;
   instructions: string;
   time_limit: string;
   number_of_questions: string;
   active: true;
   groups: [
        {
            id: string;
            public: string;
            name: string;
            description: string;
            billing_period: string
        }
    ]

   constructor() { 
   }
} 