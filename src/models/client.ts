export class Client {

   id: string;
   first_name: string;
   last_name: string;
   sex_id: string;
   date_of_birth:string;
   notes: [
    {
        id: string;
        client_id: string;
        title: string;
        body: string;
        created: string;
        modified: string;
        added: string
    }
];
responses: [
    {
        id: string;
        client_id: string;
        assessment_id: string;
        local_time: string;
        answers: string;
        time_taken:string;
        score: string;
        sensitivity: string;
        specificity: string;
        interpretation: string;
        created: string;
        added: string;
        assessment_short_name: string;
    }
]
   constructor(){
//    constructor(public id: string,
//    public first_name: string,
//    public last_name: string,
//    public sex_id: string,
//    public date_of_birth:string) { 
   }
 
}