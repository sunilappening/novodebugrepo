webpackJsonp([12],{

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Settings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
var Settings = (function () {
    function Settings(storage, defaults) {
        this.storage = storage;
        this.SETTINGS_KEY = '_settings';
        this._defaults = defaults;
    }
    Settings.prototype.load = function () {
        var _this = this;
        return this.storage.get(this.SETTINGS_KEY).then(function (value) {
            if (value) {
                _this.settings = value;
                return _this._mergeDefaults(_this._defaults);
            }
            else {
                return _this.setAll(_this._defaults).then(function (val) {
                    _this.settings = val;
                });
            }
        });
    };
    Settings.prototype._mergeDefaults = function (defaults) {
        for (var k in defaults) {
            if (!(k in this.settings)) {
                this.settings[k] = defaults[k];
            }
        }
        return this.setAll(this.settings);
    };
    Settings.prototype.merge = function (settings) {
        for (var k in settings) {
            this.settings[k] = settings[k];
        }
        return this.save();
    };
    Settings.prototype.setValue = function (key, value) {
        this.settings[key] = value;
        return this.storage.set(this.SETTINGS_KEY, this.settings);
    };
    Settings.prototype.setAll = function (value) {
        return this.storage.set(this.SETTINGS_KEY, value);
    };
    Settings.prototype.getValue = function (key) {
        return this.storage.get(this.SETTINGS_KEY)
            .then(function (settings) {
            return settings[key];
        });
    };
    Settings.prototype.save = function () {
        return this.setAll(this.settings);
    };
    Object.defineProperty(Settings.prototype, "allSettings", {
        get: function () {
            return this.settings;
        },
        enumerable: true,
        configurable: true
    });
    return Settings;
}());
Settings = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], Object])
], Settings);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Clients; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Clients = (function () {
    function Clients(http, api) {
        var _this = this;
        this.http = http;
        this.api = api;
        this.defaultItem = {
            "id": "84748",
            "user_id": "6324",
            "first_name": "Generic",
            "last_name": "Client",
            "sex_id": "3",
            "date_of_birth": "1800-01-01",
            "active": "1",
        };
        this.api.getClients('clients').subscribe(function (res) {
            _this.clients = res;
        }, function (err) {
            console.log(err);
        });
    }
    Clients.prototype.getClients = function (params) {
        if (!params) {
            return this.clients;
        }
        //return this.clients.map(client => client.filter((item) => {
        return this.clients.filter(function (item) {
            return (item.first_name.toLowerCase().indexOf(params.toLowerCase()) > -1);
        });
    };
    Clients.prototype.addClient = function (client) {
        var _this = this;
        var seq = this.api.addClient('clients', client).share();
        seq
            .map(function (res) { return res.json().data.client; })
            .subscribe(function (res) {
            console.log(res);
            // If the API returned a successful response, push the response in the clientsList
            _this.clients.push(res);
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    Clients.prototype.deleteClient = function (item) {
        this.clients.splice(this.clients.indexOf(item), 1);
    };
    Clients.prototype.addNote = function (note) {
        return this.api.addNote('clients', note);
        // let seq =  this.api.addNote('clients', note).share();
        // seq
        //   .map(res => res.json().data.note)
        //   .subscribe(res => {
        //   }, err => {
        //     console.error('ERROR', err);
        //   });
        // return seq;
    };
    Clients.prototype.editNote = function (note) {
        return this.api.updateNote('notes', note);
        //let seq =  this.api.updateNote('notes', note).share();
        // seq
        //   .map(res => res.json().data.note)
        //   .subscribe(res => {
        //   }, err => {
        //     console.error('ERROR', err);
        //   });
        // return seq;
    };
    return Clients;
}());
Clients = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__api__["a" /* Api */]])
], Clients);

//# sourceMappingURL=clients.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContentPage = (function () {
    function ContentPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return ContentPage;
}());
ContentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-content',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/content/content.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Content\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <p>\n    This is a perfect starting point for a page with primarily text content. The\n    body is padded nicely and ready for prose.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/content/content.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], ContentPage);

//# sourceMappingURL=content.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__client_client__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__report_report__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__questions_questions__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { InAppBrowser } from '@ionic-native/in-app-browser';
// import { DocumentViewer,DocumentViewerOptions } from '@ionic-native/document-viewer';





var MainPage = (function () {
    function MainPage(navCtrl, items, modalCtrl, viewCtrl, user, navParams, app) {
        this.navCtrl = navCtrl;
        this.items = items;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.user = user;
        this.navParams = navParams;
        this.app = app;
        this.questionsPage = __WEBPACK_IMPORTED_MODULE_6__questions_questions__["a" /* QuestionsPage */];
        //let userData = localStorage.getItem('userData');
        this.userData = JSON.parse(localStorage.getItem('userData'));
    }
    /**
     * The view loaded, let's query our news and recentActivities for the list
     */
    MainPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.items.getNews(this.userData).subscribe(function (res) {
            _this.currentNews = res.news;
            _this.recentActivities = res.alerts;
        }, function (err) {
            // Log errors if any
            console.log(err);
        });
    };
    MainPage.prototype.administer = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__client_client__["a" /* ClientPage */], { administer: 'administer' });
        modal.present();
    };
    MainPage.prototype.scheduleAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__client_client__["a" /* ClientPage */], { administer: 'schedule' });
        modal.present();
    };
    MainPage.prototype.emailAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__client_client__["a" /* ClientPage */], { administer: 'email' });
        modal.present();
    };
    MainPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * Delete an item from the list of items.
     */
    MainPage.prototype.deleteItem = function (item) {
        this.items.delete(item);
    };
    MainPage.prototype.splitTextName = function (detail) {
        var stringToSplit = detail;
        var x = stringToSplit.split(" ");
        return x[0] + " " + x[1];
    };
    MainPage.prototype.splitTextStatus = function (detail) {
        var stringToSplit = detail;
        var x = stringToSplit.split(" ");
        return x[3] + " " + x[5];
    };
    MainPage.prototype.itemSelected = function (url) {
        //const browser = this.iab.create(url,'_self',{location:'no'});
        //InAppBrowser.open(url, "_system", "location=true");
        //window.open(url, '_blank', 'location=no');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__report_report__["a" /* ReportPage */], { url: url });
        // let myModal = this.modalCtrl.create(ReportPage);
        // myModal.present();
        // this.document.viewDocument(url, 'application/pdf', this.options);
    };
    MainPage.prototype.logout = function () {
        var _this = this;
        localStorage.clear();
        setTimeout(function () { return _this.backToWelcome(); }, 1000);
    };
    MainPage.prototype.backToWelcome = function () {
        var root = this.app.getRootNav();
        root.popToRoot();
    };
    return MainPage;
}());
MainPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-main-page',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/main-page/main-page.html"*/'<!-- <ion-header>\n	<ion-navbar>\n		<ion-title>\n			Ionic Blank\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n -->\n<ion-content>\n\n	<div id="main" class="home-page">\n		<div class="home-header px-2">\n			<div class="container-fluid">\n				<div class="row">\n					<div class="col-md-9">\n						<img src="http://placehold.it/40x40" alt="" class="float-left mr-3">\n						<p class="lead font-large mt-0">Welcome to NosoPsych, {{userData.first_name}}</p>\n					</div>\n					<div class="col-md-3 text-right">\n\n						<a class="font-medium font-weight-bold color-white" [navPush]="questionsPage">Questions</a>\n						<a href="" class="font-medium font-weight-bold color-white d-block mt-2" (click)="logout()">Log out</a>\n					</div>\n				</div>\n			</div>\n		</div>\n		<div class="home-actions">\n			<div class="bg-overlay"></div>\n			<div class="container-fluid">\n				<div class="row px-2">\n					<div class="col-md-4">\n						<div class="home-action">\n							<img src="./assets/images/svg/home-administer.svg" (click)="administer()" >\n							<h3 class="font-large mt-3 mb-0">Administer</h3>\n						</div>\n					</div>\n					<div class="col-md-4">\n						<div class="home-action">\n							<img src="./assets/images/svg/home-email.svg" (click)="emailAssessment()">\n							<h3 class="font-large mt-3 mb-0">Email Assessment</h3>\n						</div>\n					</div>\n					<div class="col-md-4">\n						<div class="home-action" (click)="scheduleAssessment()">\n							<img src="./assets/images/svg/home-schedule.svg">\n							<h3 class="font-large mt-3 mb-0">Schedule Assessment</h3>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n		<div class="activity_news mt-5 px-2">\n			<div class="container-fluid">\n				<div class="row">\n					<div class="col-md-6">\n						<h4>Recent Acitvity</h4>\n						<div class="activity">\n                            <ion-list inset class="list-group">\n                                <button ion-item *ngFor="let item of recentActivities " (click)="itemSelected(item.link)" class="list-group-item">\n                                    <img *ngIf=" item.label == \'Assessment Completed\'" class="check" src="./assets/images/tick.png">\n                                    <img *ngIf=" item.label == \'Assessment Pending\'" class="check" src="./assets/images/email-grey.png">\n                                    \n                                    <!--<ion-icon *ngIf=" item.label == \'Assessment Completed\'" name="checkmark-circle" item-start></ion-icon>\n                                    <ion-icon *ngIf=" item.label == \'Assessment Pending\'" name="mail" item-start></ion-icon>-->\n                                    <strong>{{ splitTextName(item.detail) }}</strong>\n                                    <span>{{ splitTextStatus(item.detail) }}</span>\n                                    <!--<p>{{item.detail}}</p>-->\n                                </button>\n                            </ion-list>\n						</div>\n					</div>\n					<div class="col-md-6">\n						<h4>News</h4>\n						<div class="news">\n                            <ion-list class="list-group">\n                                <button ion-item *ngFor="let item of currentNews " (click)="itemSelected(item)" class="list-group-item">\n                                    <!--<h2>{{item.title}}</h2>-->\n                                    <span>{{item.content}}</span>\n                                </button>\n                            </ion-list>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n		<div style="padding-top:70px;"></div>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/main-page/main-page.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Items */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__providers_user__["a" /* User */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
], MainPage);

//# sourceMappingURL=main-page.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TabsPage = (function () {
    function TabsPage(navCtrl, translateService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.translateService = translateService;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__pages__["c" /* Tab1Root */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__pages__["d" /* Tab2Root */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__pages__["e" /* Tab3Root */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__pages__["f" /* Tab4Root */];
        this.tab1Title = " ";
        this.tab2Title = " ";
        this.tab3Title = " ";
        this.tab4Title = " ";
        translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE', 'TAB4_TITLE']).subscribe(function (values) {
            _this.tab1Title = values['TAB1_TITLE'];
            _this.tab2Title = values['TAB2_TITLE'];
            _this.tab3Title = values['TAB3_TITLE'];
            _this.tab4Title = values['TAB4_TITLE'];
        });
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-tabs',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/tabs/tabs.html"*/'<ion-tabs>\n	<div class="tabs-wrapper">\n		<ion-tab [root]="tab1Root" [tabTitle]="tab1Title" tabIcon="ios-home-outline"></ion-tab>\n		<ion-tab [root]="tab2Root" [tabTitle]="tab2Title" tabIcon="ios-people-outline"></ion-tab>\n		<ion-tab [root]="tab3Root" [tabTitle]="tab3Title" tabIcon="ios-clipboard-outline"></ion-tab>\n		<ion-tab [root]="tab4Root" [tabTitle]="tab4Title" tabIcon="ios-settings-outline"></ion-tab>		\n	</div>\n</ion-tabs>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/tabs/tabs.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
var WelcomePage = (function () {
    function WelcomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    WelcomePage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    WelcomePage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
    };
    return WelcomePage;
}());
WelcomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-welcome',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/welcome/welcome.html"*/'<ion-content scroll="false">\n  <div class="splash-bg"></div>\n  <div class="splash-info">\n    <div class="splash-logo"></div>\n    <div class="splash-intro">\n      {{ \'WELCOME_INTRO\' | translate }}\n    </div>\n  </div>\n  <div padding>\n    <button ion-button block (click)="signup()" class="signup">{{ \'SIGNUP\' | translate }}</button>\n    <button ion-button block (click)="login()" class="login">{{ \'LOGIN\' | translate }}</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/welcome/welcome.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], WelcomePage);

//# sourceMappingURL=welcome.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the QuestionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var QuestionsPage = (function () {
    function QuestionsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    QuestionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QuestionsPage');
    };
    return QuestionsPage;
}());
QuestionsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-questions',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/questions/questions.html"*/'<!--\n  Generated template for the QuestionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content>\n	<div class="questions-page">\n		<div class="questions-page-header p-2">\n			<div class="container-fluid">\n				<div class="row">\n					<div class="col-md-4 text-left">\n						Administrator\n					</div>\n					<div class="col-md-4 text-center">\n						<strong>ASSQ</strong>\n					</div>\n				</div>\n			</div>\n		</div>\n		<div class="questions-page-body p-2">\n			<div class="instructions">\n				<p>Instructions</p>\n				<p class="font-medium">\n					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident dignissimos quidem odio, corrupti nemo molestiae quo. Nostrum quibusdam, necessitatibus ullam?\n				</p>\n			</div>\n			<div class="questions-grouped pt-3">\n				<!-- Adds Option Heading for Questions -->\n				<div class="row px-3">\n					<div class="col-md-6"></div>\n					<div class="col-md-6 radio-header">\n						<div class="row text-center">\n							<div class="col radio-group-header">No</div>\n							<div class="col radio-group-header">Somewhat</div>\n							<div class="col radio-group-header">Yes</div>\n						</div>\n					</div>\n				</div>\n				<!-- // Adds Option Heading for Questions -->\n				<!-- Questions Body for Above Heading. No of Options(Radio Buttons) Must Match Above Options -->\n				<div class="questions-body">\n					<!-- Inline Radio Button Question -->\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">1.Is Old-fashion or precocious</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="firstquestion">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<!-- // Inline Radio Button Question ends Here -->\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">2. Is Old-fashion or precocious</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="secondquestion">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">3. Lorem ipsum dolor sit amet.</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="thirsquestion">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">4. Lorem ipsum dolor sit amet, consectetur.</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="fourthquestion">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">5. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="fifthquestion">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					\n					<!-- Block Question. I:e Option is one per line and stacked above each other.  -->\n					<div class="question questions-block">\n						<div class="row">							\n							<div class="col-md-12">\n								<p class="mt-0">6. Behavioral Questions</p>\n								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta voluptatem fugit sed laboriosam autem eveniet laudantium error? Nam, inventore alias.</p>\n							</div>\n							<div class="col-md-12 answers pt-3">\n								<ion-item item-left>\n								    <ion-radio  value="yes" item-left></ion-radio >\n								    <ion-label>Never</ion-label>\n								  </ion-item>\n\n								  <ion-item>\n								    <ion-radio  value="no" item-left></ion-radio >\n								    <ion-label>Once a Month</ion-label>\n								  </ion-item>\n							</div>\n						</div>\n					</div>\n					<!-- // Block Question Ends here -->\n				</div>\n				<!-- // Question Body Ends Here -->\n\n				<!-- Adds Option Heading for Questions -->\n				<div class="row pt-2 px-3">\n					<div class="col-md-6"></div>\n					<div class="col-md-6 radio-header">\n						<div class="row text-center">\n							<div class="col radio-group-header">Never</div>\n							<div class="col radio-group-header">Sometimes</div>\n							<div class="col radio-group-header">Often</div>\n							<div class="col radio-group-header">Always</div>\n						</div>\n					</div>\n				</div>\n				<!-- // Adds Option Heading for Questions -->\n\n				<div class="questions-body">\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">7. Is Old-fashion or precocious</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="firstquestion2">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								    <ion-radio class="col" value="fourth"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<div class="question questions-radio-inline">\n						<div class="row">							\n							<div class="col-md-6">8. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>\n							<div class="col-md-6 p-0">\n								<ion-list class="row" radio-group [(ngModel)]="secondquestion2">\n								    <ion-radio class="col" value="first" checked></ion-radio>\n								    <ion-radio class="col" value="second"></ion-radio>\n								    <ion-radio class="col" value="third"></ion-radio>\n								    <ion-radio class="col" value="fourth"></ion-radio>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n					<div class="question questions-block">\n						<div class="row">							\n							<div class="col-md-12">\n								<p class="mt-0">6. Behavioral Questions</p>\n								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta voluptatem fugit sed laboriosam autem eveniet laudantium error? Nam, inventore alias.</p>\n							</div>\n							<div class="col-md-12 answers pt-3">\n								<ion-list radio-group [(ngModel)]="questionmultiple">\n\n								  <ion-item item-left>\n								    <ion-radio  value="yes" item-left></ion-radio >\n								    <ion-label>Never</ion-label>\n								  </ion-item>\n\n								  <ion-item>\n								    <ion-radio  value="no" item-left></ion-radio >\n								    <ion-label>Once a Month</ion-label>\n								  </ion-item>\n\n								  <ion-item>\n								    <ion-radio  value="sometimes" item-left></ion-radio >\n								    <ion-label>2-3 Times a day</ion-label>\n								  </ion-item>\n								  <ion-item>\n								    <ion-radio  value="once" item-left></ion-radio >\n								    <ion-label>Once a week</ion-label>\n								  </ion-item>\n								  <ion-item>\n								    <ion-radio  value="once-day" item-left></ion-radio >\n								    <ion-label>Once a day or more</ion-label>\n								  </ion-item>\n								</ion-list>\n							</div>\n						</div>\n					</div>\n				</div>\n\n				<div class="questions-body">\n					<!-- Seekbar Question -->\n					<div class="question question-seekbar">\n						<div class="row">\n							<!-- Left Placeholder Image -->\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5"> <!-- Replace Image source -->\n							</div>\n							<!-- // Left Placeholder Image ends-->\n							<div class="col-md-8 text-center">\n								<ion-label for="">How are you?</ion-label>\n								   <ion-range min="20" max="80" step="2" [(ngModel)]="secondseekbar"></ion-range>\n							</div>\n							<!-- Right Placeholder Image -->\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5"> <!-- Replace Image source -->\n							</div>\n							<!-- // Right Placeholder Image -->\n						</div>\n					</div>\n					<!-- // Seekbar Question ends -->\n					<div class="question question-seekbar">\n						<div class="row">\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5">\n							</div>\n							<div class="col-md-8 text-center">\n								<ion-label for="">How are things in my family?</ion-label>\n								   <ion-range min="20" max="80" step="2" [(ngModel)]="firstseekbar"></ion-range>\n							</div>\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5">\n							</div>\n						</div>\n					</div>\n					<div class="question question-seekbar">\n						<div class="row">\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5">\n							</div>\n							<div class="col-md-8 text-center">\n								<ion-label for="">How good is everything?</ion-label>\n								   <ion-range min="20" max="80" step="2" [(ngModel)]="thirdseekbar"></ion-range>\n							</div>\n							<div class="col-md-2 text-center">\n								<img src="http://placehold.it/60x60" class="pt-5">\n							</div>\n						</div>\n					</div>\n					<!-- Input Question -->\n					<div class="question question-input">\n						<div class="row">\n							<div class="col-md-12">\n								<p class="mt-0">7. If you are afraid of something else please write down what it is.</p>\n							    <ion-input placeholder="Enter Response Here"></ion-input>\n							</div>\n						</div>\n					</div>\n					<!-- // Input Question ends -->\n				</div>\n			</div>\n			<div class="submit-btn text-center">\n				<button class="text-center w-75 py-2 mt-5">Submit Assessment</button>\n			</div>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/questions/questions.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], QuestionsPage);

//# sourceMappingURL=questions.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssessmentTabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AssessmentTabPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AssessmentTabPage = (function () {
    function AssessmentTabPage(navCtrl, navParams, viewCtrl, items) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.items = items;
        this.clientId = this.navParams.get('clientId');
        this.assessmentsTag = [];
        this.assessmentNameTag = [];
        this.selectedAssessments = {
            short_name: 'First Name',
            extended_name: 'Last Name'
        };
    }
    AssessmentTabPage.prototype.ionViewDidLoad = function () {
        console.log('value of clientId passed' + this.clientId);
        this.assessments = this.items.getAssessments();
    };
    AssessmentTabPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    AssessmentTabPage.prototype.assessmentSelected = function (item) {
        //little hack to display the tag name
        this.assessmentsTag.push(item);
        this.assessmentNameTag.push(item.short_name);
        this.assessment = item;
    };
    AssessmentTabPage.prototype.onChange = function (val) {
        this.assessmentsTag.pop();
        this.assessmentNameTag.pop();
    };
    AssessmentTabPage.prototype.filterAssessments = function (ev) {
        var val = ev.target.value;
        this.assessments = this.items.getAssessments(val);
        return this.assessments;
    };
    return AssessmentTabPage;
}());
AssessmentTabPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-assessment-tab',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/assessment-tab/assessment-tab.html"*/'<!--\n  Generated template for the ClientPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<!-- <ion-content>\n  <ion-row>\n    <ion-col>\n      <ion-navbar>\n        <ion-title>Assessments</ion-title>\n      </ion-navbar>\n\n     <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n    <ion-list inset> \n    <button ion-item *ngFor="let item of assessments " (click)="assessmentSelected(item)">\n            <span>{{item.short_name}}</span>\n            <span> {{item.extended_name}}</span>\n          </button>\n  </ion-list>\n    </ion-col>\n    <ion-col>\n      <assessment-details [assessment]="assessment"></assessment-details>\n    </ion-col>\n  </ion-row>\n</ion-content> -->\n\n<ion-content>\n  <div id="main" class="inner-page single-assessment-page">\n    <div class="container-fluid">\n      <div class="row">\n        <div class="col-md-4">\n          <div class="inner-page-nav assessment-nav row">\n              <div class="inner-page-header assessment-header w-100 p-2">\n                <h4 class="text-center my-2">Assessments</h4>\n                <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n              </div>\n  \n              <div class="inner-page-list list-scroller w-100">\n								<ion-scroll >\n                    <button class="li-block w-100 common-assessments text-left" (click)="commonAssessmentSelected()">\n                      <p class="mb-3">Common Assessment</p>\n                      <p class="small">Quick Access to common tests</p>\n                    </button>\n                    <ion-list>\n                      <button class="li-block w-100" [class.active]="i==1" text-left *ngFor="let item of assessments;let i = index" (click)="assessmentSelected(item)">\n                        <p class="mb-3">{{item.short_name}}</p>\n                        <p class="small">{{ item.extended_name }} </p>\n                      </button>\n                    </ion-list>\n                  </ion-scroll>\n              </div>\n          </div>\n        </div>\n        <div class="col-md-8">\n          <assessment-details [assessment]="assessment"></assessment-details>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/assessment-tab/assessment-tab.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Items */]])
], AssessmentTabPage);

//# sourceMappingURL=assessment-tab.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientTabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__client_create_client_create__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ClientTabPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ClientTabPage = (function () {
    function ClientTabPage(navCtrl, navParams, modalCtrl, items, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.items = items;
        this.viewCtrl = viewCtrl;
        this.iters = new Array(10);
    }
    ClientTabPage.prototype.ionViewDidLoad = function () {
        //ionViewDidEnter(){
        this.currentItems = this.items.getClients();
        this.client = this.currentItems[0];
    };
    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    ClientTabPage.prototype.addClient = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__client_create_client_create__["a" /* ClientCreatePage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                console.log('inside addClient before calling the api');
                _this.items.addClient(item);
            }
        });
        addModal.present();
    };
    ClientTabPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    ClientTabPage.prototype.clientSelected = function (item) {
        //let client = {clientId: item.id, first_name: item.first_name};
        this.client = item;
    };
    ClientTabPage.prototype.filterClients = function (ev) {
        var val = ev.target.value;
        this.currentItems = this.items.getClients(val);
        return this.currentItems;
    };
    ClientTabPage.prototype.calculateAge = function (birthday) {
        if (birthday) {
            var timeDiff = Math.abs(Date.now() - Date.parse(birthday));
            //Used Math.floor instead of Math.ceil
            //so 26 years and 140 days would be considered as 26, not 27.
            return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
        }
    };
    return ClientTabPage;
}());
ClientTabPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-client-tab',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/client-tab/client-tab.html"*/'<!--\n	Generated template for the ClientPage page.\n\n	See http://ionicframework.com/docs/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n\n<!-- <ion-content>\n	<ion-row>\n		<ion-col>\n			<ion-navbar>\n				<ion-title>Select Client</ion-title>\n\n				<ion-buttons right>\n					<button ion-button icon-only (click)="addClient()">\n							<ion-icon name="add"></ion-icon>\n							</button>\n				</ion-buttons>\n			</ion-navbar>\n			<ion-searchbar (ionInput)="filterClients($event)"></ion-searchbar>\n			<ion-list>\n				<button ion-item *ngFor="let item of currentItems " (click)="clientSelected(item)">\n					<ion-row>\n						<ion-col><h2><b>{{item.first_name}} {{item.last_name}}</b></h2></ion-col>\n						<ion-col>{{ calculateAge(item.date_of_birth) }} Years old</ion-col>\n						</ion-row>\n					</button>\n			</ion-list>\n		</ion-col>\n		<ion-col>\n			<client-details [client]="client"></client-details>\n		</ion-col>\n	</ion-row>\n</ion-content> -->\n\n<ion-content>\n<div id="main" class="inner-page clients-page">\n	<div class="container-fluid">\n		<div class="row">\n			<div class="col-md-4">\n				<div class="inner-page-nav row">\n						<div class="inner-page-header w-100 p-2">\n							<h4 class="text-center my-2">Clients\n									<span class="float-right">\n										<button class="btn-icon" (click)="addClient()">\n											<img src="./assets/images/new-user.png">\n										</button>\n									</span>\n							</h4>\n								\n							\n							<ion-searchbar (ionInput)="filterClients($event)"></ion-searchbar>\n							<!--<ion-searchbar\n							  [(ngModel)]="myInput"\n							  [showCancelButton]="shouldShowCancel">\n							</ion-searchbar>-->\n\n							<!-- <div class="inner-page-search">\n								<input type="text" id="search" class="form-control">\n								<label for="search" class="search-label">\n									<img src="./assets/images/svg/search-icon.svg" alt="">\n									<span>Search</span>\n								</label>\n							</div> -->\n						</div>\n\n						<div class="inner-page-list w-100">\n							<ion-list>\n								<!-- <button class="li-block w-100" [class.active]="i==2" text-left *ngFor="let item of currentItems;let i = index" (click)="clientSelected(item)"> -->\n									<button class="li-block w-100" [class.active]="item.id == client.id" text-left *ngFor="let item of currentItems" (click)="clientSelected(item)">\n									<p class="mb-3">{{item.first_name}} {{item.last_name}}</p>\n									<p class="small">{{ calculateAge(item.date_of_birth) }} years old</p>\n								</button>\n							</ion-list>\n						</div>\n				</div>\n			</div>\n			<div class="col-md-8">\n				<client-details [client]="client"></client-details>\n			</div>\n		</div>\n	</div>\n</div>\n</ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/client-tab/client-tab.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_providers__["a" /* Items */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], ClientTabPage);

//# sourceMappingURL=client-tab.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AccountPage = (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.text = 'plan';
        this.isPlanSelected = true;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage.prototype.planSelected = function (item) {
        this.text = item;
        this.isPlanSelected = true;
        this.isAccountSelected = false;
        this.isFeedbackSelected = false;
        this.isHelpSelected = false;
    };
    AccountPage.prototype.accountSelected = function (item) {
        this.text = item;
        this.isAccountSelected = true;
        this.isFeedbackSelected = false;
        this.isHelpSelected = false;
        this.isPlanSelected = false;
    };
    AccountPage.prototype.feedbackSelected = function (item) {
        this.text = item;
        this.isFeedbackSelected = true;
        this.isPlanSelected = false;
        this.isHelpSelected = false;
        this.isAccountSelected = false;
    };
    AccountPage.prototype.helpSelected = function (item) {
        this.text = item;
        this.isHelpSelected = true;
        this.isFeedbackSelected = false;
        this.isPlanSelected = false;
        this.isAccountSelected = false;
    };
    return AccountPage;
}());
AccountPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-account',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/account/account.html"*/'<ion-content>\n\n  <div id="main" class="inner-page clients-page">\n\n    <div class="container-fluid">\n      <div class="row">\n        <div class="col-md-4">\n          <div class="inner-page-nav assessment-nav row">\n            <div class="inner-page-header assessment-header w-100 p-0">\n                <h4 class="text-center">Account</h4>\n            </div>\n\n            <div class="inner-page-list assessment-list w-100">\n              <button class="li-block w-100" [class.active]="isPlanSelected" text-left (click)="planSelected(\'plan\')">\n 								<p class="mb-3">Plan</p>\n 								<p class="small">Upgrade to get more tests and client</p>\n 							</button>\n              <div class="li-block" [class.active]="isAccountSelected" (click)="accountSelected(\'account\')">\n                <p class="mb-1">Account Details</p>\n                <p class="small">Personal details, email, passcode</p>\n              </div>\n\n              <div class="li-block" [class.active]="isFeedbackSelected" (click)="feedbackSelected(\'feedback\')">\n                <p class="mb-1">Feedback</p>\n                <p class="small">Suggestions, errors, feature requests</p>\n              </div>\n              <div class="li-block"[class.active]="isHelpSelected" (click)="helpSelected(\'help\')">\n                <p class="mb-1">Help</p>\n                <p class="small">Userguide, FAQ, TCs, Privacy Policy</p>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class="col-md-8">\n          <upgrade-plan [text]="text"></upgrade-plan>\n        </div>\n\n        </div>\n      </div>\n    </div>\n  \n\n</ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/account/account.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], AccountPage);

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_items__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_clients__ = __webpack_require__(107);
/* unused harmony reexport User */
/* unused harmony reexport Api */
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__settings__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_3__providers_items__["a"]; });
/* unused harmony reexport Clients */






//# sourceMappingURL=providers.js.map

/***/ }),

/***/ 150:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 150;

/***/ }),

/***/ 205:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/account/account.module": [
		394,
		11
	],
	"../pages/add-note/add-note.module": [
		395,
		10
	],
	"../pages/administer/administer.module": [
		385,
		9
	],
	"../pages/assessment-tab/assessment-tab.module": [
		392,
		8
	],
	"../pages/client-tab/client-tab.module": [
		393,
		7
	],
	"../pages/client/client.module": [
		389,
		6
	],
	"../pages/email/email.module": [
		388,
		5
	],
	"../pages/questions/questions.module": [
		391,
		4
	],
	"../pages/report/report.module": [
		390,
		3
	],
	"../pages/schedule/schedule.module": [
		387,
		2
	],
	"../pages/select-assessment/select-assessment.module": [
		386,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 205;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Items; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__clients__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assessments__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Items = (function () {
    function Items(http, api, clients, assessments) {
        this.http = http;
        this.api = api;
        this.clients = clients;
        this.assessments = assessments;
    }
    Items.prototype.emailAssessments = function (item) {
        return this.api.scheduleAssessments('email', item);
    };
    Items.prototype.getNews = function (params) {
        return this.api.getNews('notifications', params);
    };
    Items.prototype.getClients = function (params) {
        return this.clients.getClients(params);
    };
    Items.prototype.getClientNotesAndResponses = function (clientId) {
        return this.api.getClientNotesAndResponses('clients', clientId);
    };
    Items.prototype.getAssessments = function (params) {
        return this.assessments.getAssessments(params);
    };
    Items.prototype.query = function (params) {
        return this.api.getNews('news', params);
    };
    Items.prototype.addClient = function (client) {
        return this.clients.addClient(client);
    };
    Items.prototype.delete = function (item) {
    };
    Items.prototype.addNote = function (item) {
        return this.clients.addNote(item);
    };
    Items.prototype.editNote = function (item) {
        return this.clients.editNote(item);
    };
    Items.prototype.getReport = function (link) {
        return this.api.getReportPDF(link);
    };
    return Items;
}());
Items = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_5__clients__["a" /* Clients */], __WEBPACK_IMPORTED_MODULE_6__assessments__["a" /* Assessments */]])
], Items);

//# sourceMappingURL=items.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Assessments; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Assessments = (function () {
    function Assessments(api) {
        var _this = this;
        this.api = api;
        this.defaultItem = {
            "id": "84748",
            "user_id": "6324",
            "first_name": "Generic",
            "last_name": "Client",
            "sex_id": "3",
            "date_of_birth": "1800-01-01",
            "active": "1",
        };
        this.api.getAssessments('assessments', '0.5').subscribe(function (res) {
            _this.assessments = res;
        }, function (err) {
            console.log(err);
        });
    }
    Assessments.prototype.getAssessments = function (params) {
        if (!params) {
            return this.assessments;
        }
        return this.assessments.filter(function (item) {
            return (item.short_name.toLowerCase().indexOf(params.toLowerCase()) > -1);
        });
    };
    return Assessments;
}());
Assessments = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api__["a" /* Api */]])
], Assessments);

//# sourceMappingURL=assessments.js.map

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CardsPage = (function () {
    function CardsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cardItems = [
            {
                user: {
                    avatar: 'assets/img/marty-avatar.png',
                    name: 'Marty McFly'
                },
                date: 'November 5, 1955',
                image: 'assets/img/advance-card-bttf.png',
                content: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.',
            },
            {
                user: {
                    avatar: 'assets/img/sarah-avatar.png.jpeg',
                    name: 'Sarah Connor'
                },
                date: 'May 12, 1984',
                image: 'assets/img/advance-card-tmntr.jpg',
                content: 'I face the unknown future, with a sense of hope. Because if a machine, a Terminator, can learn the value of human life, maybe we can too.'
            },
            {
                user: {
                    avatar: 'assets/img/ian-avatar.png',
                    name: 'Dr. Ian Malcolm'
                },
                date: 'June 28, 1990',
                image: 'assets/img/advance-card-jp.jpg',
                content: 'Your scientists were so preoccupied with whether or not they could, that they didn\'t stop to think if they should.'
            }
        ];
    }
    return CardsPage;
}());
CardsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cards',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/cards/cards.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ \'CARDS_TITLE\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card *ngFor="let item of cardItems">\n\n    <ion-item>\n      <ion-avatar item-start>\n        <img [src]="item.user.avatar">\n      </ion-avatar>\n      <h2>{{item.user.name}}</h2>\n      <p>{{item.date}}</p>\n    </ion-item>\n\n    <img [src]="item.image">\n\n    <ion-card-content>\n      <p>{{item.content}}</p>\n    </ion-card-content>\n\n    <ion-row>\n      <ion-col>\n        <button ion-button color="primary" clear small icon-start>\n            <ion-icon name=\'thumbs-up\'></ion-icon>\n            12 Likes\n          </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button color="primary" clear small icon-start>\n            <ion-icon name=\'text\'></ion-icon>\n            4 Comments\n          </button>\n      </ion-col>\n      <ion-col center text-center>\n        <ion-note>\n          11h ago\n        </ion-note>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/cards/cards.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], CardsPage);

//# sourceMappingURL=cards.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__content_content__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MenuPage = (function () {
    function MenuPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__content_content__["a" /* ContentPage */];
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Sign in', component: __WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */] },
            { title: 'Signup', component: __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */] }
        ];
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('Hello MenuPage Page');
    };
    MenuPage.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    return MenuPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
], MenuPage.prototype, "nav", void 0);
MenuPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-menu',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/menu/menu.html"*/'<ion-menu [content]="content">\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<ion-nav #content [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/menu/menu.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], MenuPage);

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemDetailPage = (function () {
    function ItemDetailPage(navCtrl, navParams, items) {
        this.navCtrl = navCtrl;
        //this.item = navParams.get('item') || items.defaultItem;
    }
    return ItemDetailPage;
}());
ItemDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-item-detail',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/item-detail/item-detail.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ item.name }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="item-profile" text-center #profilePic [style.background-image]="\'url(\' + item.profilePic + \')\'">\n  </div>\n\n  <div class="item-detail" padding>\n    <h2>{{item.name}}</h2>\n    <p>{{item.about}}</p>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/item-detail/item-detail.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Items */]])
], ItemDetailPage);

//# sourceMappingURL=item-detail.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
var SettingsPage = SettingsPage_1 = (function () {
    function SettingsPage(navCtrl, settings, formBuilder, navParams, translate) {
        this.navCtrl = navCtrl;
        this.settings = settings;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.translate = translate;
        this.settingsReady = false;
        this.profileSettings = {
            page: 'profile',
            pageTitleKey: 'SETTINGS_PAGE_PROFILE'
        };
        this.page = 'main';
        this.pageTitleKey = 'SETTINGS_TITLE';
        this.subSettings = SettingsPage_1;
    }
    SettingsPage.prototype._buildForm = function () {
        var _this = this;
        var group = {
            option1: [this.options.option1],
            option2: [this.options.option2],
            option3: [this.options.option3]
        };
        switch (this.page) {
            case 'main':
                break;
            case 'profile':
                group = {
                    option4: [this.options.option4]
                };
                break;
        }
        this.form = this.formBuilder.group(group);
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.settings.merge(_this.form.value);
        });
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        // Build an empty form for the template to render
        this.form = this.formBuilder.group({});
    };
    SettingsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // Build an empty form for the template to render
        this.form = this.formBuilder.group({});
        this.page = this.navParams.get('page') || this.page;
        this.pageTitleKey = this.navParams.get('pageTitleKey') || this.pageTitleKey;
        this.translate.get(this.pageTitleKey).subscribe(function (res) {
            _this.pageTitle = res;
        });
        this.settings.load().then(function () {
            _this.settingsReady = true;
            _this.options = _this.settings.allSettings;
            _this._buildForm();
        });
    };
    SettingsPage.prototype.ngOnChanges = function () {
        console.log('Ng All Changes');
    };
    return SettingsPage;
}());
SettingsPage = SettingsPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-settings',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/settings/settings.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ pageTitle }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <form [formGroup]="form" *ngIf="settingsReady">\n    <ion-list *ngIf="page == \'main\'">\n      <ion-item>\n        <ion-label>{{ \'SETTINGS_OPTION1\' | translate }}</ion-label>\n        <ion-toggle formControlName="option1"></ion-toggle>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>{{ \'SETTINGS_OPTION2\' | translate }}</ion-label>\n        <ion-input formControlName="option2"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>{{ \'SETTINGS_OPTION3\' | translate }}</ion-label>\n        <ion-select formControlName="option3">\n          <ion-option value="1" checked="true">1</ion-option>\n          <ion-option value="2">2</ion-option>\n          <ion-option value="3">3</ion-option>\n        </ion-select>\n      </ion-item>\n\n      <button ion-item [navPush]="subSettings" [navParams]="profileSettings">\n        {{ \'SETTINGS_PROFILE_BUTTON\' | translate }}\n      </button>\n    </ion-list>\n\n    <ion-list *ngIf="page == \'profile\'">\n      <ion-item>\n        <ion-label>{{ \'SETTINGS_OPTION4\' | translate }}</ion-label>\n        <ion-input formControlName="option4"></ion-input>\n      </ion-item>\n    </ion-list>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/settings/settings.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings__["a" /* Settings */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
], SettingsPage);

var SettingsPage_1;
//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__welcome_welcome__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutorialPage = (function () {
    function TutorialPage(navCtrl, menu, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.showSkip = true;
        translate.get(["TUTORIAL_SLIDE1_TITLE",
            "TUTORIAL_SLIDE1_DESCRIPTION",
            "TUTORIAL_SLIDE2_TITLE",
            "TUTORIAL_SLIDE2_DESCRIPTION",
            "TUTORIAL_SLIDE3_TITLE",
            "TUTORIAL_SLIDE3_DESCRIPTION",
        ]).subscribe(function (values) {
            console.log('Loaded values', values);
            _this.slides = [
                {
                    title: values.TUTORIAL_SLIDE1_TITLE,
                    description: values.TUTORIAL_SLIDE1_DESCRIPTION,
                    image: 'assets/img/ica-slidebox-img-1.png',
                },
                {
                    title: values.TUTORIAL_SLIDE2_TITLE,
                    description: values.TUTORIAL_SLIDE2_DESCRIPTION,
                    image: 'assets/img/ica-slidebox-img-2.png',
                },
                {
                    title: values.TUTORIAL_SLIDE3_TITLE,
                    description: values.TUTORIAL_SLIDE3_DESCRIPTION,
                    image: 'assets/img/ica-slidebox-img-3.png',
                }
            ];
        });
    }
    TutorialPage.prototype.startApp = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__welcome_welcome__["a" /* WelcomePage */], {}, {
            animate: true,
            direction: 'forward'
        });
    };
    TutorialPage.prototype.onSlideChangeStart = function (slider) {
        this.showSkip = !slider.isEnd();
    };
    TutorialPage.prototype.ionViewDidEnter = function () {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
    };
    TutorialPage.prototype.ionViewWillLeave = function () {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    };
    return TutorialPage;
}());
TutorialPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-tutorial',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/tutorial/tutorial.html"*/'<ion-header no-shadow>\n  <ion-navbar>\n    <ion-buttons end *ngIf="showSkip">\n      <button ion-button (click)="startApp()" color="primary">{{ \'TUTORIAL_SKIP_BUTTON\' | translate}}</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <ion-slides pager="true" (ionSlideWillChange)="onSlideChangeStart($event)">\n    <ion-slide *ngFor="let slide of slides">\n      <img [src]="slide.image" class="slide-image" />\n      <h2 class="slide-title" [innerHTML]="slide.title"></h2>\n      <p [innerHTML]="slide.description"></p>\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/img/ica-slidebox-img-4.png" class="slide-image" />\n      <h2 class="slide-title">{{ \'TUTORIAL_SLIDE4_TITLE\' | translate }}</h2>\n      <button ion-button icon-end large clear (click)="startApp()">\n        {{ \'TUTORIAL_CONTINUE_BUTTON\' | translate }}\n        <ion-icon name="arrow-forward"></ion-icon>\n      </button>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/tutorial/tutorial.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
], TutorialPage);

//# sourceMappingURL=tutorial.js.map

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(277);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* unused harmony export provideSettings */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_pdf_viewer__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_pdf_viewer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_pdf_viewer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_tags_input__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_cards_cards__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_content_content__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_item_detail_item_detail__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_main_page_main_page__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_login_login__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_menu_menu__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_client_tab_client_tab__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_assessment_tab_assessment_tab__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_settings_settings__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_signup_signup__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_tabs_tabs__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_tutorial_tutorial__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_welcome_welcome__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_schedule_schedule__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_email_email__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_client_client__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_client_create_client_create__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_select_assessment_select_assessment__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_account_account__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_administer_administer__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_report_report__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_add_note_add_note__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_questions_questions__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__components_client_details_client_details__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_assessment_details_assessment_details__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__components_upgrade_plan_upgrade_plan__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_items__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_settings__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_user__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_clients__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_assessments__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_splash_screen__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_status_bar__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ngx_translate_core__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__ngx_translate_http_loader__ = __webpack_require__(382);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { InAppBrowser } from '@ionic-native/in-app-browser';
// import { DocumentViewer,DocumentViewerOptions } from '@ionic-native/document-viewer';


//import { TagsInputModule } from 'ionic2-tags-input';







































// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_43__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
function provideSettings(storage) {
    /**
     * The Settings provider takes a set of default settings for your app.
     *
     * You can add new settings options at any time. Once the settings are saved,
     * these values will not overwrite the saved values (this can be done manually if desired).
     */
    return new __WEBPACK_IMPORTED_MODULE_36__providers_settings__["a" /* Settings */](storage, {
        option1: true,
        option2: 'Ionitron J. Framework',
        option3: '3',
        option4: 'Hello'
    });
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cards_cards__["a" /* CardsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_content_content__["a" /* ContentPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_client_create_client_create__["a" /* ClientCreatePage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_select_assessment_select_assessment__["a" /* SelectAssessmentPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_item_detail_item_detail__["a" /* ItemDetailPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_main_page_main_page__["a" /* MainPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_menu_menu__["a" /* MenuPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_client_tab_client_tab__["a" /* ClientTabPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_assessment_tab_assessment_tab__["a" /* AssessmentTabPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_tutorial_tutorial__["a" /* TutorialPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_welcome_welcome__["a" /* WelcomePage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_schedule_schedule__["a" /* SchedulePage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_email_email__["a" /* EmailPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_client_client__["a" /* ClientPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_account_account__["a" /* AccountPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_administer_administer__["a" /* AdministerPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_report_report__["a" /* ReportPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_add_note_add_note__["a" /* AddNotePage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_questions_questions__["a" /* QuestionsPage */],
            __WEBPACK_IMPORTED_MODULE_31__components_client_details_client_details__["a" /* ClientDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_32__components_assessment_details_assessment_details__["a" /* AssessmentDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_33__components_upgrade_plan_upgrade_plan__["a" /* UpgradePlanComponent */],
            __WEBPACK_IMPORTED_MODULE_3_ng2_pdf_viewer__["PdfViewerComponent"],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
            //TagsInputModule,
            __WEBPACK_IMPORTED_MODULE_5_ionic_tags_input__["a" /* IonTagsInputModule */],
            __WEBPACK_IMPORTED_MODULE_42__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_42__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: HttpLoaderFactory,
                    deps: [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]]
                }
            }),
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], { tabsPlacement: 'top' }, {
                links: [
                    { loadChildren: '../pages/administer/administer.module#AdministerPageModule', name: 'AdministerPage', segment: 'administer', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/select-assessment/select-assessment.module#SelectAssessmentPageModule', name: 'SelectAssessmentPage', segment: 'select-assessment', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/schedule/schedule.module#SchedulePageModule', name: 'SchedulePage', segment: 'schedule', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/email/email.module#EmailPageModule', name: 'EmailPage', segment: 'email', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/client/client.module#ClientPageModule', name: 'ClientPage', segment: 'client', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/report/report.module#ReportPageModule', name: 'ReportPage', segment: 'report', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/questions/questions.module#QuestionsPageModule', name: 'QuestionsPage', segment: 'questions', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/assessment-tab/assessment-tab.module#AssessmentTabPageModule', name: 'AssessmentTabPage', segment: 'assessment-tab', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/client-tab/client-tab.module#ClientTabPageModule', name: 'ClientTabPage', segment: 'client-tab', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-note/add-note.module#AddNotePageModule', name: 'AddNotePage', segment: 'add-note', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cards_cards__["a" /* CardsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_content_content__["a" /* ContentPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_client_create_client_create__["a" /* ClientCreatePage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_select_assessment_select_assessment__["a" /* SelectAssessmentPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_item_detail_item_detail__["a" /* ItemDetailPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_main_page_main_page__["a" /* MainPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_menu_menu__["a" /* MenuPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_client_tab_client_tab__["a" /* ClientTabPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_assessment_tab_assessment_tab__["a" /* AssessmentTabPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_settings_settings__["a" /* SettingsPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_tutorial_tutorial__["a" /* TutorialPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_welcome_welcome__["a" /* WelcomePage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_schedule_schedule__["a" /* SchedulePage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_email_email__["a" /* EmailPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_client_client__["a" /* ClientPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_account_account__["a" /* AccountPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_administer_administer__["a" /* AdministerPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_report_report__["a" /* ReportPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_add_note_add_note__["a" /* AddNotePage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_questions_questions__["a" /* QuestionsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_34__providers_api__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_35__providers_items__["a" /* Items */],
            __WEBPACK_IMPORTED_MODULE_37__providers_user__["a" /* User */],
            __WEBPACK_IMPORTED_MODULE_38__providers_clients__["a" /* Clients */],
            __WEBPACK_IMPORTED_MODULE_39__providers_assessments__["a" /* Assessments */],
            __WEBPACK_IMPORTED_MODULE_40__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_41__ionic_native_status_bar__["a" /* StatusBar */],
            { provide: __WEBPACK_IMPORTED_MODULE_36__providers_settings__["a" /* Settings */], useFactory: provideSettings, deps: [__WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]] },
            // Keep this to enable Ionic's runtime error handling during development
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 285:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 301:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 302:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 303:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_content_content__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pages__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_main_page_main_page__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_search_search__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_settings_settings__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tutorial_tutorial__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_welcome_welcome__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_schedule_schedule__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_email_email__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_client_client__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_client_create_client_create__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_select_assessment_select_assessment__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_administer_administer__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_report_report__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_add_note_add_note__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_providers__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ngx_translate_core__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


























var MyApp = (function () {
    function MyApp(translate, platform, settings, statusBar, splashScreen) {
        this.translate = translate;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_pages__["a" /* FirstRunPage */];
        this.pages = [
            { title: 'Tutorial', component: __WEBPACK_IMPORTED_MODULE_14__pages_tutorial_tutorial__["a" /* TutorialPage */] },
            { title: 'Welcome', component: __WEBPACK_IMPORTED_MODULE_15__pages_welcome_welcome__["a" /* WelcomePage */] },
            { title: 'Tabs', component: __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__["a" /* TabsPage */] },
            { title: 'Cards', component: __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards__["a" /* CardsPage */] },
            { title: 'Content', component: __WEBPACK_IMPORTED_MODULE_5__pages_content_content__["a" /* ContentPage */] },
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */] },
            { title: 'Signup', component: __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */] },
            { title: 'Main Page', component: __WEBPACK_IMPORTED_MODULE_7__pages_main_page_main_page__["a" /* MainPage */] },
            { title: 'Menu', component: __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__["a" /* MenuPage */] },
            { title: 'Settings', component: __WEBPACK_IMPORTED_MODULE_11__pages_settings_settings__["a" /* SettingsPage */] },
            { title: 'Search', component: __WEBPACK_IMPORTED_MODULE_10__pages_search_search__["a" /* SearchPage */] },
            { title: 'Schedule', component: __WEBPACK_IMPORTED_MODULE_16__pages_schedule_schedule__["a" /* SchedulePage */] },
            { title: 'Email', component: __WEBPACK_IMPORTED_MODULE_17__pages_email_email__["a" /* EmailPage */] },
            { title: 'Client', component: __WEBPACK_IMPORTED_MODULE_18__pages_client_client__["a" /* ClientPage */] },
            { title: 'Create Client', component: __WEBPACK_IMPORTED_MODULE_19__pages_client_create_client_create__["a" /* ClientCreatePage */] },
            { title: 'Select Assessment', component: __WEBPACK_IMPORTED_MODULE_20__pages_select_assessment_select_assessment__["a" /* SelectAssessmentPage */] },
            { title: 'Administer', component: __WEBPACK_IMPORTED_MODULE_21__pages_administer_administer__["a" /* AdministerPage */] },
            { title: 'Report', component: __WEBPACK_IMPORTED_MODULE_22__pages_report_report__["a" /* ReportPage */] },
            { title: 'Add Note', component: __WEBPACK_IMPORTED_MODULE_23__pages_add_note_add_note__["a" /* AddNotePage */] },
        ];
        this.initTranslate();
    }
    MyApp.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.initTranslate = function () {
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('en');
        if (this.translate.getBrowserLang() !== undefined) {
            this.translate.use(this.translate.getBrowserLang());
        }
        else {
            this.translate.use('en'); // Set your language here
        }
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: "<ion-menu [content]=\"content\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>Pages</ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n      <ion-list>\n        <button menuClose ion-item *ngFor=\"let p of pages\" (click)=\"openPage(p)\">\n          {{p.title}}\n        </button>\n      </ion-list>\n    </ion-content>\n\n  </ion-menu>\n  <ion-nav #content [root]=\"rootPage\"></ion-nav>"
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_25__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_24__providers_providers__["b" /* Settings */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_detail_item_detail__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchPage = (function () {
    function SearchPage(navCtrl, navParams, items) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = items;
        this.currentItems = [];
    }
    /**
     * Perform a service for the proper items.
     */
    SearchPage.prototype.getItems = function (ev) {
        var val = ev.target.value;
        if (!val || !val.trim()) {
            this.currentItems = [];
            return;
        }
        this.currentItems = this.items.query({
            name: val
        });
    };
    /**
     * Navigate to the detail page for this item.
     */
    SearchPage.prototype.openItem = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__item_detail_item_detail__["a" /* ItemDetailPage */], {
            item: item
        });
    };
    return SearchPage;
}());
SearchPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/search/search.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ \'SEARCH_TITLE\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-searchbar (ionInput)="getItems($event)" placeholder="{{ \'SEARCH_PLACEHOLDER\' | translate }}"></ion-searchbar>\n  <ion-list>\n    <button ion-item (click)="openItem(item)" *ngFor="let item of currentItems">\n      <ion-avatar item-start>\n        <img [src]="item.profilePic" />\n      </ion-avatar>\n      <h2>{{item.name}}</h2>\n      <p>{{item.about}}</p>\n      <ion-note item-end *ngIf="item.note">{{item.note}}</ion-note>\n    </button>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/search/search.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_providers__["a" /* Items */]])
], SearchPage);

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_client__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_client_client__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_client_create_client_create__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_add_note_add_note__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ClientDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var ClientDetailsComponent = (function () {
    function ClientDetailsComponent(modalCtrl, items) {
        this.modalCtrl = modalCtrl;
        this.items = items;
        //this.text = "No Client Selected";
    }
    ClientDetailsComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        for (var propName in changes) {
            if (propName === 'client') {
                var client = changes['client'].currentValue;
                if (typeof client === 'undefined') {
                    //do nothing
                }
                else {
                    this.items.getClientNotesAndResponses(client.id).subscribe(function (res) {
                        _this.clientNotes = res.notes;
                        _this.pastResults = res.responses;
                    }, function (err) {
                        console.log(err);
                    });
                }
            }
        }
    };
    ClientDetailsComponent.prototype.administer = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'administer' });
        modal.present();
    };
    ClientDetailsComponent.prototype.emailAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'email' });
        modal.present();
    };
    ClientDetailsComponent.prototype.scheduleAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'schedule' });
        modal.present();
    };
    ClientDetailsComponent.prototype.addNote = function (clientId) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_add_note_add_note__["a" /* AddNotePage */], { clientId: clientId });
        modal.onDidDismiss(function (item) {
            if (item) {
                // let addedNote = this.items.addNote({clientId: clientId, title:item.title, body:item.body});
                var seq = _this.items.addNote({ clientId: clientId, title: item.title, body: item.body }).share();
                seq
                    .map(function (res) { return res.json().data.note; })
                    .subscribe(function (res) {
                    console.log(res);
                    _this.clientNotes.push(res);
                }, function (err) {
                    console.error('ERROR', err);
                });
            }
        });
        modal.present();
    };
    ClientDetailsComponent.prototype.noteSelected = function (note, indexValue) {
        var _this = this;
        console.log("index value of selected note is " + indexValue);
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_add_note_add_note__["a" /* AddNotePage */], { note: note });
        modal.onDidDismiss(function (item) {
            if (item) {
                var obj = { title: item.title, body: item.body, client_id: note.client_id, id: note.id };
                //let updatedNote = this.items.editNote(obj);
                var seq = _this.items.editNote(obj).share();
                seq
                    .map(function (res) { return res.json().data.note; })
                    .subscribe(function (res) {
                    console.log(res);
                    _this.clientNotes[indexValue] = res;
                }, function (err) {
                    console.error('ERROR', err);
                });
            }
        });
        modal.present();
    };
    ClientDetailsComponent.prototype.editClient = function (item) {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_client_create_client_create__["a" /* ClientCreatePage */], { client: item });
        addModal.onDidDismiss(function (item) {
            if (item) {
                console.log('inside editClient before calling the api');
                _this.items.addClient(item);
            }
        });
        addModal.present();
    };
    return ClientDetailsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_client__["a" /* Client */])
], ClientDetailsComponent.prototype, "client", void 0);
ClientDetailsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'client-details',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/components/client-details/client-details.html"*/'<div class="row" *ngIf="client">\n	<div class="inner-page-content common-assessment-content w-100">\n		<div class="single-client-page">\n			<div class="client-info-block mt-4">\n				<div class="client-info">\n					<div class="row">\n						<div class="col-md-8">\n							<img src="./assets/images/none.png" class="float-left m-3 mt-4" alt="" style="width:70px;height:70px;">\n							<!-- <div class="client-details my-3"> -->\n								<div class="client-details mt-3">\n								<h3 class="font-large font-weight-bold ">{{client.first_name}}</h3>\n								<p class="mb-0">\n									<span class="font-weight-bold">Age: </span>\n									<span class="value"> {{client.date_of_birth}}</span>\n								</p>\n								<p class="mb-0">\n									<span class="font-weight-bold">Email: </span>\n									<span class="value"> {{client.email != \'\' ? client.email : \'No email provided\'}}</span>\n								</p>\n							</div>\n						</div>\n						<div class="col-md-4 text-right">\n							<img src="./assets/images/svg/edit.svg" alt="" class="m-2" (click)="editClient(client)">\n							<br/>\n							<button class="btn btn-primary mr-3 mt-3 px-4" (click)="administer()">\n								<img src="./assets/images/svg/ipad.svg" alt="Administer">\n								Administer\n							</button>\n						</div>\n					</div>\n				</div>\n				<div class="client-actions">\n					<div class="row">\n						<div class="col-md-6 pr-0 text-center">\n								<button class="text-center w-100" (click)="emailAssessment()">\n									<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="Email">\n									Email\n								</button>\n						</div>\n						<div class="col-md-6 pl-0 text-center">\n							<button class="text-center w-100" (click)="scheduleAssessment()">\n								<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="Schedule">\n								Schedule\n							</button>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class="scheduling mt-4">\n				<div class="header">\n					<h3 class="float-left font-lead m-0">Scheduling</h3>\n					<div class="scheduling-nav float-right">\n						<a href="">View Sent Logs</a>\n						<a href="">View Future Logs</a>\n					</div>\n					<div class="clearfix"></div>\n				</div>\n				<div class="scheduling-list">\n					<div class="single-scheduling">\n						<div class="status mb-2">\n							<span class="signal success"></span>\n							<span class="font-weight-bold">In Progress</span>\n							<img src="./assets/images/svg/edit.svg" alt="" class="float-right">\n							<!-- <span class="signal error"></span>\n							<span class="signal warning"></span> -->\n						</div>\n						<p class="info">\n							Repeated <span class="highlight">DASS-21</span> scheduled on <span class="highlight">21 July 2017</span> every <span class="highlight">Week</span> for <span class="highlight">8</span> cycles to <span class="highlight">emailtest@email.com</span>\n						</p>\n					</div>\n					<div class="single-scheduling">\n						<div class="status mb-2">\n							<span class="signal error"></span>\n							<span class="font-weight-bold">Expired</span>\n							<img src="./assets/images/svg/edit.svg" alt="" class="float-right">\n							<!-- <span class="signal error"></span>\n							<span class="signal warning"></span> -->\n						</div>\n						<p class="info">\n							Repeated <span class="highlight">DASS-21</span> scheduled on <span class="highlight">21 July 2017</span> every <span class="highlight">Week</span> for <span class="highlight">8</span> cycles to <span class="highlight">emailtest@email.com</span>\n						</p>\n					</div>\n				</div>\n			</div>\n\n			<div class="past-results results mt-4">\n				<div class="header">\n					<h3 class="font-lead">Past Results</h3>\n				</div>\n				<ion-list class="list-group">\n					<button class="list-group-item" ion-item *ngFor="let item of pastResults" (click)="resultSelected(item)">\n						<ion-row>\n								<ion-col>\n									<span class="date">{{item.added}}</span>\n									<span class="code">{{item.assessment_short_name}}</span>\n								</ion-col>\n								<ion-col text-right>\n									<span class="message">Contacted Via Email <img class="minus-top-2" src="./assets/images/svg/email-grey.svg" alt=""></span>\n								</ion-col>\n							</ion-row>\n						</button>\n				</ion-list>\n			</div>\n\n			<div class="past-results results mt-4">\n				<div class="header">\n					<h3 class="font-lead">Notes</h3>\n				</div>\n				<ion-list class="list-group">\n					<button class="list-group-item" ion-item *ngFor="let item of clientNotes; let i = index" (click)="noteSelected(item,i)">\n						<ion-row>\n								<ion-col>\n									<span class="date">{{item.added}}</span>\n									<span class="code">{{item.body}}</span>\n								</ion-col>\n							</ion-row>\n						</button>\n				</ion-list>\n				<button class="btn btn-primary mt-2 px-4" (click)="addNote(client.id)">Add Note</button>\n				<div class="pad30"></div>\n			</div>\n		</div>\n	</div>\n</div>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/components/client-details/client-details.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__providers_providers__["a" /* Items */]])
], ClientDetailsComponent);

//# sourceMappingURL=client-details.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Client; });
var Client = (function () {
    function Client() {
        //    constructor(public id: string,
        //    public first_name: string,
        //    public last_name: string,
        //    public sex_id: string,
        //    public date_of_birth:string) { 
    }
    return Client;
}());

//# sourceMappingURL=client.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssessmentDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_assessment__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_client_client__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AssessmentDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var AssessmentDetailsComponent = (function () {
    function AssessmentDetailsComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
        console.log('Hello ClientDetailsComponent Component');
    }
    AssessmentDetailsComponent.prototype.administer = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'administer' });
        modal.present();
    };
    AssessmentDetailsComponent.prototype.emailAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'email' });
        modal.present();
    };
    AssessmentDetailsComponent.prototype.scheduleAssessment = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_client_client__["a" /* ClientPage */], { administer: 'schedule' });
        modal.present();
    };
    return AssessmentDetailsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_assessment__["a" /* Assessment */])
], AssessmentDetailsComponent.prototype, "assessment", void 0);
AssessmentDetailsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'assessment-details',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/components/assessment-details/assessment-details.html"*/'<!-- Generated template for the AssessmentDetailsComponent component -->\n<!-- <div *ngIf="assessment">\n      <h2>{{assessment.extended_name}} details!</h2>\n      <div><label>id: </label>{{assessment.id}}</div>\n      <div>\n        <label>name: </label>\n        <input [(ngModel)]="assessment.extended_name" placeholder="name"/>\n      </div>\n</div> -->\n\n<div class="row" *ngIf="assessment">\n					<div class="inner-page-content common-assessment-content w-100">\n						<div class="assessment-info-block mt-4">\n							<div class="assessment-info">\n								<div class="row">\n									<div class="col-md-8">\n										<img src="./assets/images/svg/test-card-blue.svg" class="float-left m-3" alt="">\n										<div class="assessment-details mt-3">\n											<h3 class="font-large font-weight-bold">{{assessment.extended_name}}</h3>\n										</div>\n									</div>\n									<div class="col-md-4 mt-4 text-right">\n										<button class="btn btn-primary mr-3 mt-5 px-4" (click)="administer()">\n											<img src="./assets/images/svg/ipad.svg" class="minus-top-2" alt="">\n											Administer\n										</button>\n									</div>\n								</div>\n							</div>\n							<div class="assessment-actions">\n								<div class="row">\n									<div class="col-md-6 pr-0 text-center">\n											<button class="text-center w-100" (click)="emailAssessment()">\n												<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">\n												Email\n											</button>\n									</div>\n									<div class="col-md-6 pl-0 text-center">\n										<button class="text-center w-100" (click)="scheduleAssessment()">\n												<img class="minus-top-2" src="./assets/images/svg/email-black.svg" alt="">\n											Subscribe\n										</button>\n									</div>\n								</div>\n							</div>\n						</div>\n						<div class="single-assessment-details">\n							<h4>Description</h4>\n							<p>{{assessment.description}} </p>\n\n							<h4>Validity and Reliability</h4>\n							<p>{{assessment.validity}}</p>\n\n							<h4>Interpretation</h4>\n							<p>{{assessment.interpretation}}</p>\n\n							<h4>Developer</h4>\n							<p>{{assessment.developer}}</p>\n\n							<h4>Number Of Questions</h4>\n							<p>{{assessment.number_of_questions}}</p>\n\n							<h4>Time limit</h4>\n							<p>{{assessment.time_limit}}</p>\n\n							<h4>References</h4>\n							<p>{{assessment.references}}</p>\n						</div>\n					</div>\n  </div>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/components/assessment-details/assessment-details.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
], AssessmentDetailsComponent);

//# sourceMappingURL=assessment-details.js.map

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Api; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Api is a generic REST Api handler. Set your API url first.
 */
var Api = (function () {
    function Api(http) {
        this.http = http;
        //url: string = 'https://novopsych.com/api';
        this.url = 'http://localhost:8100/v1';
    }
    Api.prototype.resetPassword = function (endpoint, body) {
        var response = this.http.post(this.url + '/' + endpoint, JSON.stringify(body))
            .map(function (res) { return res.json().data; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.login = function (endpoint, credentials) {
        var _this = this;
        this.credentials = credentials;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Authorization', "Basic ".concat(btoa(_this.credentials.username + ':' + _this.credentials.password)));
            var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
            _this.http.get(_this.url + '/' + endpoint, options)
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    Api.prototype.register = function (endpoint, body) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.url + '/' + endpoint, JSON.stringify(body), options);
    };
    Api.prototype.addClient = function (endpoint, body) {
        var response = this.http.post(this.url + '/' + endpoint, JSON.stringify(body), this.createAuthorizationHeader());
        //.map(res => res.json().data)
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
        return response;
    };
    Api.prototype.addNote = function (endpoint, body) {
        var response = this.http.post(this.url + '/' + endpoint + '/' + body.clientId + '/' + 'notes', JSON.stringify(body), this.createAuthorizationHeader());
        // .map(res => <Note>res.json().data.note)
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
        return response;
    };
    Api.prototype.updateNote = function (endpoint, body) {
        var response = this.http.put(this.url + '/' + endpoint + '/' + body.id, JSON.stringify(body), this.createAuthorizationHeader());
        //.map(res => res.json().data)
        //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
        return response;
    };
    Api.prototype.getClients = function (endpoint) {
        var response = this.http.get(this.url + '/' + endpoint, this.createAuthorizationHeader())
            .map(function (res) { return res.json().data.clients; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.getClientNotesAndResponses = function (endpoint, clientId) {
        var response = this.http.get(this.url + '/' + endpoint + '/' + clientId, this.createAuthorizationHeader())
            .map(function (res) { return res.json().data.client; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.getNews = function (endpoint, body) {
        var response = this.http.get(this.url + '/' + endpoint, this.createAuthorizationHeader())
            .map(function (res) { return res.json().data; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.getAssessments = function (endpoint, version) {
        var assessmentVersion = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* URLSearchParams */]();
        assessmentVersion.set('version', version);
        var options = this.createAuthorizationHeader();
        options.search = assessmentVersion;
        var response = this.http.get(this.url + '/' + endpoint + '/', options)
            .map(function (res) { return res.json().data.assessments; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.scheduleAssessments = function (endpoint, body) {
        var response = this.http.post(this.url + '/' + endpoint, JSON.stringify(body), this.createAuthorizationHeader())
            .map(function (res) { return res.json().data; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.getScheduledAssessments = function (endpoint, scheduleId) {
        var response = this.http.get(this.url + '/' + endpoint + '/' + scheduleId, this.createAuthorizationHeader())
            .map(function (res) { return res.json().data.schedule; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
        return response;
    };
    Api.prototype.deleteSchedule = function (endpoint, scheduleId) {
        return this.http.delete(this.url + '/' + endpoint + '/' + scheduleId, this.createAuthorizationHeader());
    };
    Api.prototype.createAuthorizationHeader = function () {
        var userId = localStorage.getItem('userId');
        var password = localStorage.getItem('password');
        if (userId && password) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Authorization', "Basic ".concat(btoa(userId + ':' + password)));
            return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        }
    };
    Api.prototype.put = function (endpoint, body, options) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    };
    Api.prototype.delete = function (endpoint, options) {
        return this.http.delete(this.url + '/' + endpoint, options);
    };
    Api.prototype.patch = function (endpoint, body, options) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    };
    Api.prototype.getReportPDF = function (link) {
        var userId = localStorage.getItem('userId');
        var password = localStorage.getItem('password');
        return this.http.get(link, {
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With",
                "Authorization": "Basic ".concat(btoa(userId + ':' + password))
            }),
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].ArrayBuffer // YOU NEED THAT
        });
    };
    return Api;
}());
Api = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], Api);

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Assessment; });
var Assessment = (function () {
    function Assessment() {
    }
    return Assessment;
}());

//# sourceMappingURL=assessment.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpgradePlanComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the UpgradePlanComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var UpgradePlanComponent = (function () {
    function UpgradePlanComponent() {
    }
    return UpgradePlanComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], UpgradePlanComponent.prototype, "text", void 0);
UpgradePlanComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'upgrade-plan',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/components/upgrade-plan/upgrade-plan.html"*/'<div class="row" *ngIf="text === \'plan\'">\n    <div class="inner-page-content common-assessment-content w-100">\n                        <div class="page-header">\n                            <h3 class="font-lead m-0">Plan</h3>\n                        </div>\n                        <div class="assessments">\n                            <div class="row">\n                                <div class="col">\n                                    <table class="table plans-table">\n                                        <thead>\n                                            <tr>\n                                                <th>Features</th>\n                                                <th class="free">Free</th>\n                                                <th>Basic</th>\n                                                <th>Pro</th>\n                                                <th>Unlimited</th>\n                                                <th>Practice</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>No. of Clients</span>\n                                                </td>\n                                                <td>Up to 40</td>\n                                                <td>Up to 80</td>\n                                                <td>Up to 300</td>\n                                                <td>Unlimited</td>\n                                                <td>Unlimited</td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>No. of test administrations</span>\n                                                </td>\n                                                <td>Up to 40 per month</td>\n                                                <td>Up to 80 per month</td>\n                                                <td>Up to 300 per month</td>\n                                                <td>Unlimited</td>\n                                                <td>Unlimited</td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>No. of practitioner accounts</span>\n                                                </td>\n                                                <td>Up to 40</td>\n                                                <td>Up to 80</td>\n                                                <td>Up to 300</td>\n                                                <td>Unlimited</td>\n                                                <td>Unlimited</td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Access to all world class psychometric assessments</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Instant scoring saving you valuable time</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Scores presented as percentiles for easy interpretation</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Graph symptoms over time to track progress in treatment</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Email assessment to clients so they can complete at home</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Schedule regular assessments via email</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr>\n                                                <td>\n                                                    <img src="http://placehold.it/40x40">\n                                                    <span>Export results as CSV into excel</span>\n                                                </td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                                <td><span class="check"></span></td>\n                                            </tr>\n                                            <tr class="actions">\n                                                <td></td>\n                                                <td>\n                                                    <button class="btn free">\n                                                        <p class="price">$0</p>\n                                                        <p class="small">per month*</p>\n                                                    </button>\n                                                </td>\n                                                <td>\n                                                    <button class="btn btn-success">\n                                                        <p class="price">$15</p>\n                                                        <p class="small">per month*</p>\n                                                    </button>\n                                                </td>\n                                                <td>\n                                                    <button class="btn btn-success">\n                                                        <p class="price">$30</p>\n                                                        <p class="small">per month*</p>\n                                                    </button>\n                                                </td>\n                                                <td>\n                                                    <button class="btn btn-success">\n                                                        <p class="price">$60</p>\n                                                        <p class="small">per month*</p>\n                                                    </button>\n                                                </td>\n                                                <td>\n                                                    <button class="btn btn-success">\n                                                        <p class="price">$199</p>\n                                                        <p class="small">per month*</p>\n                                                    </button>\n                                                </td>\n\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n</div>\n\n<div class="row" *ngIf="text === \'account\'">\n    <div class="inner-page-content common-assessment-content w-100">\n        <div class="page-header">\n            <h3 class="font-lead m-0">Account Details</h3>\n        </div>\n        <div class="assessments">\n            <div class="row">\n                <div class="col">\n                    <form action="">\n                        <div class="form-group">\n                            <label for="">Title</label>\n                            <div class="radio-group">\n                                <div class="radio-input">\n                                    <input type="radio" id="radio" name="radio">\n                                    <label for="radio">Dr</label>\n                                </div>\n                                <div class="radio-input">\n                                    <input type="radio" id="radio2" name="radio" checked="">\n                                    <label for="radio2">Mr</label>\n                                </div>\n                                <div class="radio-input">\n                                    <input type="radio" id="radio3" name="radio">\n                                    <label for="radio3">Mrs</label>\n                                </div>\n                                <div class="radio-input">\n                                    <input type="radio" id="radio4" name="radio">\n                                    <label for="radio4">Ms</label>\n                                </div>\n                                <div class="radio-input">\n                                    <input type="radio" id="radio5" name="radio">\n                                    <label for="radio5">Prof</label>\n                                </div>\n                            </div>\n                        </div>\n                        <div class="row">\n                            <div class="col">\n                                <div class="form-group">\n                                    <label for="">First Name</label>\n                                    <input type="text" class="form-control">\n                                </div>\n                            </div>\n                            <div class="col">\n                                <div class="form-group">\n                                    <label for="">Last Name</label>\n                                    <input type="text" class="form-control">\n                                </div>\n                            </div>\n                        </div>\n                        <div class="row">\n                            <div class="col">\n                                <div class="form-group">\n                                    <label for="">Email</label>\n                                    <input type="text" class="form-control">\n                                </div>\n                            </div>\n                            <div class="col profession">\n                                <div class="form-group">\n                                <ion-item>\n                                    <ion-label stacked>Profession</ion-label>\n                                    <ion-select name="profession">\n                                        <ion-option value="1">Psychologist</ion-option>\n                                        <ion-option value="2">Psychatrist</ion-option>\n                                        <ion-option value="3">Counsllor</ion-option>\n                                        <ion-option value="4">Medical Practitioner</ion-option>            \n                                        <ion-option value="5">Psychology Student</ion-option>\n                                        <ion-option value="6">Occupational Therapist</ion-option>\n                                        <ion-option value="7">Psychiatric Nurse</ion-option>\n                                        <ion-option value="8">Social Worker</ion-option>\n                                        <ion-option value="9">Student</ion-option>            \n                                        <ion-option value="10">Researcher</ion-option>\n                                        <ion-option value="11">Other</ion-option>\n                                    </ion-select>\n                                </ion-item>\n                                </div>\n                            </div>\n                        </div>\n                        <div class="form-group mt-2">\n                            <button class="btn btn-success">Update Details</button>\n                        </div>\n                    </form>\n                    <hr class="my-4">\n                    <h4 class="legend">Security</h4>\n                    <div class="form-group mt-4">\n                        <button class="btn btn-default mr-2">Change Passcode</button>\n                        <button class="btn btn-default">Change Password</button>\n                    </div>\n                    <hr class="my-4">\n                    <h4 class="legend">Email Preferences</h4>\n                    <p>Would you like to receive reports in PDF format?</p>\n                    <div class="radio-group">\n                        <div class="radio-input">\n                            <input type="radio" id="pref" name="pref">\n                            <label for="pref">No</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" id="pref1" checked name="pref">\n                            <label for="pref1">As Link</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" id="pref2" name="pref">\n                            <label for="pref2">As Attachment</label>\n                        </div>\n                    </div>\n                    <div class="pad30"></div>\n                    <p>Would you like to receive reports in CSV format?</p>\n                    <div class="radio-group">\n                        <div class="radio-input">\n                            <input type="radio" id="csv" name="csv">\n                            <label for="csv">No</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" id="csv1" checked name="csv">\n                            <label for="csv1">As Link</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" id="csv2" name="csv">\n                            <label for="csv2">As Attachment</label>\n                        </div>\n                    </div>\n                    <div class="form-group mt-4">\n                        <button class="btn btn-success">Save preferences</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>                    \n</div>\n\n<div class="row" *ngIf="text === \'feedback\'">\n        <div class="inner-page-content common-assessment-content w-100">\n            <div class="page-header">\n                <h3 class="font-lead m-0">Feedback</h3>\n            </div>\n            <div class="assessments">\n                <div class="row">\n                    <div class="col">\n                        <form action="">\n                            <div class="form-group">\n                                <label for="">Feedback Type</label>\n                                <div class="radio-group">\n                                    <div class="radio-input">\n                                        <input type="radio" name="type" id="radio1" checked="checked">\n                                        <label for="radio1">General</label>\n                                    </div>\n                                    <div class="radio-input">\n                                        <input type="radio" name="type" id="radio2">\n                                        <label for="radio2">Report an Error</label>\n                                    </div>\n                                    <div class="radio-input">\n                                        <input type="radio" name="type" id="radio3">\n                                        <label for="radio3">Feature Request</label>\n                                    </div>\n                                    <div class="radio-input">\n                                        <input type="radio" name="type" id="radio4">\n                                        <label for="radio4">Other</label>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class="form-group">\n                                <label for="">Type Message Here</label>\n                                <textarea type="text" class="form-control" rows="8"></textarea>\n                            </div>\n                            <div class="form-group">\n                                <button class="btn btn-success">Send</button>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/components/upgrade-plan/upgrade-plan.html"*/
    })
], UpgradePlanComponent);

//# sourceMappingURL=upgrade-plan.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstRunPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return HomePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Tab1Root; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return Tab2Root; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return Tab3Root; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return Tab4Root; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__main_page_main_page__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assessment_tab_assessment_tab__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__client_tab_client_tab__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__account_account__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(56);






// The page the user lands on after opening the app and without a session
var FirstRunPage = __WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */];
// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
var HomePage = __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */];
// The initial root pages for our tabs (remove if not using tabs)
var Tab1Root = __WEBPACK_IMPORTED_MODULE_0__main_page_main_page__["a" /* MainPage */];
var Tab2Root = __WEBPACK_IMPORTED_MODULE_2__client_tab_client_tab__["a" /* ClientTabPage */];
var Tab3Root = __WEBPACK_IMPORTED_MODULE_1__assessment_tab_assessment_tab__["a" /* AssessmentTabPage */];
var Tab4Root = __WEBPACK_IMPORTED_MODULE_3__account_account__["a" /* AccountPage */];
//# sourceMappingURL=pages.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__client_create_client_create__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__select_assessment_select_assessment__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__schedule_schedule__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__email_email__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ClientPage = (function () {
    function ClientPage(navCtrl, navParams, modalCtrl, items, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.items = items;
        this.viewCtrl = viewCtrl;
    }
    //ionViewDidLoad() {
    ClientPage.prototype.ionViewDidEnter = function () {
        this.currentItems = this.items.getClients();
        this.administer = this.navParams.get('administer');
    };
    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    ClientPage.prototype.addClient = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__client_create_client_create__["a" /* ClientCreatePage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.items.addClient(item);
            }
        });
        addModal.present();
    };
    ClientPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    ClientPage.prototype.clientSelected = function (item) {
        var client = { clientId: item.id, clientName: item.first_name + " " + item.last_name, clientEmail: item.email };
        var addModal;
        if (this.administer == 'administer') {
            addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__select_assessment_select_assessment__["a" /* SelectAssessmentPage */], client);
        }
        if (this.administer == 'schedule') {
            addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__schedule_schedule__["a" /* SchedulePage */], client);
        }
        if (this.administer == 'email') {
            addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__email_email__["a" /* EmailPage */], client);
        }
        addModal.present();
    };
    ClientPage.prototype.filterClients = function (ev) {
        var val = ev.target.value;
        this.currentItems = this.items.getClients(val);
        return this.currentItems;
    };
    ClientPage.prototype.calculateAge = function (birthday) {
        if (birthday) {
            var timeDiff = Math.abs(Date.now() - Date.parse(birthday));
            //Used Math.floor instead of Math.ceil
            //so 26 years and 140 days would be considered as 26, not 27.
            return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
        }
    };
    return ClientPage;
}());
ClientPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-client',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/client/client.html"*/'<!--\n  Generated template for the ClientPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>\n\n  <ion-navbar>\n    <ion-title>Select Client</ion-title>\n\n    <ion-buttons left>\n      <button ion-button icon-only (click)="addClient()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button icon-only (click)="cancel()" end>\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-searchbar (ionInput)="filterClients($event)"></ion-searchbar>\n  <ion-list>\n    <button ion-item *ngFor="let item of currentItems " (click)="clientSelected(item)">\n          <ion-row>\n            <ion-col><h2><b>{{item.first_name}} {{item.last_name}}</b></h2></ion-col>\n            <ion-col>{{ calculateAge(item.date_of_birth) }} Years old</ion-col>\n            </ion-row>\n          </button>\n  </ion-list>\n</ion-content>-->\n\n<!-- Select Client Modal -->\n<ion-content>\n				<div class="popup">\n					<div class="popup-dialog" role="document">\n						<div class="popup-content">\n							<div class="popup-header">\n								<div class="popup-title">\n									<div class="d-flex w-100">\n										<div class="col-md-1">\n											<button class="btn-icon" (click)="addClient()">\n												<img src="./assets/images/new-user.png">\n											</button>\n										</div>\n										<div class="col-md-10 text-center">\n											<h3>Select Client</h3>\n										</div>\n										<div class="col-md-1">\n											<button class="btn-icon" (click)="cancel()">\n												<img src="./assets/images/close.png">\n											</button>\n										</div>\n									</div>\n								</div>\n								<div class="popup-search" >\n									<ion-searchbar (ionInput)="filterClients($event)"></ion-searchbar>\n								</div>\n							</div>\n							<div class="popup-body p-0">\n								<ion-list class="list-group">\n								<!-- <button class="li-block w-100" text-left *ngFor="let item of currentItems;let i = index" (click)="clientSelected(item)" class="list-group-item"> -->\n										<button ion-item *ngFor="let item of currentItems " (click)="clientSelected(item)" class="list-group-item">\n									<!-- <p class="float-left">{{item.first_name}} {{item.last_name}}</p> -->\n									<strong class="float-left">{{item.first_name}} {{item.last_name}}</strong>\n									<!-- <p class="float-right">{{ calculateAge(item.date_of_birth) }} years old</p> -->\n									<span class="float-right">{{ calculateAge(item.date_of_birth) }} years old</span>\n								</button>\n							</ion-list>\n							</div>\n						</div>\n					</div>\n				</div>\n</ion-content>\n				<!-- // Select Client Modal -->\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/client/client.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__providers_providers__["a" /* Items */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], ClientPage);

//# sourceMappingURL=client.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Acc */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Acc = (function () {
    function Acc(name, email) {
        this.name = name;
        this.username = email;
    }
    return Acc;
}());

/**
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 * If the `status` field is not `success`, then an error is detected and returned.
 */
var User = (function () {
    function User(http, api, storage) {
        this.http = http;
        this.api = api;
        this.storage = storage;
        //this.api.url = "https://novopsych.com/api"
    }
    /**
     * Send a GET request to our login endpoint with the data
     * the user entered on the form.
     */
    User.prototype.login = function (credentials) {
        return this.api.login('login', credentials);
    };
    /**
     * Send a POST request to our signup endpoint with the data
     * the user entered on the form.
     */
    User.prototype.signup = function (accountInfo) {
        var _this = this;
        var seq = this.api.register('users', accountInfo).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.status == 'success') {
                _this._loggedIn(res);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    /**
     * Log the user out, which forgets the session
     */
    User.prototype.logout = function () {
        this.currentUser = null;
    };
    /**
     * Process a login/signup response to store user data
     */
    User.prototype._loggedIn = function (resp) {
        this.currentUser = resp.user;
    };
    User.prototype.forgotPassword = function (email) {
        return this.api.resetPassword('passwordreset', email);
    };
    return User;
}());
User = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], User);

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClientCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ClientCreatePage = (function () {
    function ClientCreatePage(navCtrl, viewCtrl, formBuilder) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.client = {
            first_name: '',
            last_name: '',
            email: '',
            date_of_birth: '',
            sex_id: ''
        };
        this.submitClient = false;
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        this.form = formBuilder.group({
            first_name: [this.client.first_name, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            last_name: [this.client.last_name, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            email: [this.client.email, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern(EMAIL_REGEXP)])],
            date_of_birth: [this.client.date_of_birth],
            sex_id: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    ClientCreatePage.prototype.ionViewDidLoad = function () {
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    ClientCreatePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    ClientCreatePage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        console.log(this.form.get('sex_id').value);
        this.viewCtrl.dismiss(this.form.value);
    };
    return ClientCreatePage;
}());
ClientCreatePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-client-create',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/client-create/client-create.html"*/'<!-- <ion-header>\n  \n    <ion-navbar>\n      <ion-title>Client Details</ion-title>\n      <ion-buttons start>\n        <button ion-button (click)="cancel()">\n          <span color="primary" showWhen="ios">\n            {{ \'CANCEL_BUTTON\' | translate }}\n          </span>\n          <ion-icon name="md-close" showWhen="android,windows"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-buttons end>\n        <button ion-button (click)="done()" [disabled]="!isReadyToSave" strong>\n          <span color="primary" showWhen="ios">\n            {{ \'DONE_BUTTON\' | translate }}\n          </span>\n          <ion-icon name="md-checkmark" showWhen="core,android,windows"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content>\n    <form *ngIf="form" [formGroup]="form" (ngSubmit)="createClient()">\n      \n      <ion-list>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label stacked>First name or ID</ion-label>\n              <ion-input type="text" formControlName="first_name"></ion-input>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <ion-label stacked>Last name or ID</ion-label>\n              <ion-input type="text" formControlName="last_name"></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n  \n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>{{ \'EMAIL\' | translate }}</ion-label>\n            <ion-input type="email" formControlName="email"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>Date of birth</ion-label>\n            <ion-datetime displayFormat="MM/DD/YYYY" min="1960" max="2002-10-31" formControlName="date_of_birth"></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n  \n        <ion-row>\n            <ion-list radio-group formControlName="sex_id">\n              <ion-list-header>\n                Gender\n              </ion-list-header>\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label>Undefined</ion-label>\n                    <ion-radio checked="true" value="1"></ion-radio>\n                  </ion-item>\n                </ion-col>\n                <ion-col>\n                  <ion-item>\n                    <ion-label>Male</ion-label>\n                    <ion-radio value="2"></ion-radio>\n                  </ion-item>\n                </ion-col>\n                <ion-col>\n                  <ion-item>\n                    <ion-label>Female</ion-label>\n                    <ion-radio value="3"></ion-radio>\n                  </ion-item>\n                </ion-col>\n                \n              </ion-row>\n            </ion-list>\n        </ion-row>\n  \n      </ion-list>\n    </form>\n  </ion-content> -->\n\n  <ion-content>\n    <form *ngIf="form" [formGroup]="form">\n    <div class="popup">\n      <div class="popup-dialog" role="document">\n        <div class="popup-content schedule_setting">\n          <div class="popup-header">\n            <div class="popup-title">\n              <div class="d-flex w-100">\n                <div class="col-md-1">\n                  <button class="btn-icon">\n                    <img src="./assets/images/left-arrow.png">\n                  </button>\n                </div>\n                <div class="col-md-10 text-center">\n                  <h3>Client Details</h3>\n                </div>\n                <div class="col-md-1">\n                  <button class="btn-icon" (click)="cancel()">\n                    <img src="./assets/images/close.png">\n                  </button>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class="popup-body pt-3">\n            <div class="row">\n              <div class="col">\n                <div class="form-group">\n                  <label for="" class="text-muted small">First Name or ID</label>\n                  <ion-input class = "form-control" type="text" formControlName="first_name"></ion-input>\n                </div>\n              </div>\n              <div class="col">\n                <div class="form-group">\n                  <label for="" class="text-muted small">Last Name or ID</label>\n                  <ion-input class = "form-control" type="text" formControlName="last_name"></ion-input>\n                </div>\n              </div>\n            </div>\n\n            <div class="row">\n              <div class="col">\n                <div class="form-group">\n                  <label for="" class="text-muted small">Email</label>\n                  <ion-input class = "form-control" type="email" formControlName="email"></ion-input>\n                </div>\n              </div>\n              <div class="col">\n                <div class="form-group">\n                  <label for="" class="text-muted small">Date of Birth</label>\n                  <ion-datetime class = "form-control" displayFormat="MM/DD/YYYY" min="1960" max="2002-10-31" formControlName="date_of_birth"></ion-datetime>\n                </div>\n              </div>\n            </div>\n\n            <div class="row">\n              <div class="col-md-8">\n                <label for="" class="text-muted small">Gender</label>\n                <div class="radio-group">\n                  <div class="radio-input bg-white">\n                    <input type="radio" value="1" id="gender1" formControlName="sex_id">\n                    <label for="gender1">Undefined</label>\n                  </div>\n                  <div class="radio-input bg-white">\n                    <input type="radio" value="2" id="gender2" formControlName="sex_id">\n                    <label for="gender2">Male</label>\n                  </div>\n                  <div class="radio-input bg-white">\n                    <input type="radio" value="3" id="gender3" formControlName="sex_id">\n                    <label for="gender3">Female</label>\n                  </div>\n                </div>\n\n              </div>\n\n              <div class="col-md-4 text-right">\n                <button class="btn-icon delete-client-btn">\n                  <img src="./assets/images/svg/trash.svg" class="minus-top-2" alt="">\n                  <span>Delete Client</span>\n                </button>\n              </div>\n            </div>\n            \n          </div>\n          <div class="popup-footer text-center">\n            <div class="note-btns text-center mt-0">\n              <button class="btn-icon mr-3" (click)="done()" [disabled]="!isReadyToSave">\n                <span>Add Client</span>\n                <img src="./assets/images/svg/start.svg" class="minus-top-2" alt="">\n              </button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    </form>\n  </ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/client-create/client-create.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
], ClientCreatePage);

//# sourceMappingURL=client-create.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_pages__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = (function () {
    function LoginPage(navCtrl, user, toastCtrl, translateService, modalCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.credentials = {
            username: '',
            password: ''
        };
        this.translateService.get('SIGNUP_ERROR').subscribe(function (value) {
            _this.signupErrorString = value;
        });
        this.translateService.get('LOGIN_ERROR').subscribe(function (value) {
            _this.loginErrorString = value;
        });
    }
    LoginPage.prototype.doSignup = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
        modal.onDidDismiss(function (item) {
            if (item) {
                _this.user.signup(item).subscribe(function (resp) {
                    //let responseData = resp;
                    //localStorage.setItem('userData', JSON.stringify(responseData.data.user))
                    _this.credentials.username = item.email;
                    _this.credentials.password = item.password;
                    _this.doLogin();
                    // this.navCtrl.push(HomePage);
                }, function (err) {
                    // Unable to sign up
                    var toast = _this.toastCtrl.create({
                        message: _this.signupErrorString,
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();
                });
            }
        });
        modal.present();
    };
    // Attempt to login in through our User service
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        if (this.credentials.username && this.credentials.password) {
            this.user.login(this.credentials).then(function (result) {
                _this.responseData = result;
                if (_this.responseData.meta.status = 'ok') {
                    localStorage.setItem('userData', JSON.stringify(_this.responseData.data.user));
                    localStorage.setItem('userId', _this.credentials.username);
                    localStorage.setItem('password', _this.credentials.password);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_pages__["b" /* HomePage */]);
                }
                else {
                    _this.presentToast(_this.loginErrorString);
                }
            }, function (err) {
                _this.presentToast(_this.loginErrorString);
            });
        }
        else {
            this.presentToast("Give username and password");
        }
    };
    LoginPage.prototype.forgotPassword = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Forgot Password?',
            message: "Please enter your email to reset your password",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'email@xyz.com'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        var res = _this.user.forgotPassword(data.email).subscribe(function (resp) {
                            _this.presentToast("Your password has been sent to your email address");
                        }, function (err) {
                            _this.presentToast("The email id doesn't exist");
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/login/login.html"*/'<ion-content>\n	<div id="main" class="login-page">\n			<div class="container">\n				<div class="row" style="padding-top:150px;">\n					<div class="col-md-6 text-center" >\n						<div class="branding-left align-middle">\n							<img src="./assets/images/svg/novopsych-logo.svg" alt="" class="pt-5" style="width:175px;">\n							<h1 class="mt-3">NovoPsych</h1>\n							<p class="lead mt-4">Administer assessments, receive reports.<br/> For Psychologists, <br/>Psychiatrists &amp; Counsellors</p>\n						</div>\n					</div>\n					<div class="col-md-6">\n						<form (submit)="doLogin()">\n							<legend>Login</legend>\n							<div class="form-group">\n								<!-- <ion-label fixed>{{ \'EMAIL\' | translate }}</ion-label> -->\n								<label for="email" class="color-white">Email</label>\n                                <ion-input class = "form-control" type="email" [(ngModel)]="credentials.username" name="email"></ion-input>\n							</div>\n\n							<div class="form-group">\n								<!-- <ion-label fixed>{{ \'PASSWORD\' | translate }}</ion-label> -->\n								<label for="password" class="color-white">Password</label>\n                                <ion-input class="form-control" type="password" [(ngModel)]="credentials.password" name="password"></ion-input>\n							</div>\n							<a href="#" class="font-small font-weight-bold color-white" (click)="forgotPassword()">Forgot Password?</a>\n							<div class="form-group pt-4">\n								<!-- <button ion-button class="btn btn-primary w-100" color="primary">{{ \'LOGIN_BUTTON\' | translate }}</button> -->\n								<button class="btn btn-primary w-100">Login</button>								\n							</div>\n							</form>\n							<hr class="my-4 mx-0 line">\n							<h4 class="my-0">New to NovoPsych?</h4>\n							<div class="form-group mt-4">\n								<!-- <button ion-button class="btn btn-primary w-100" (click)="doSignup()">{{ \'SIGNUP_BUTTON\' | translate }}</button> -->\n								<button class="btn btn-primary w-100" (click)="doSignup()">{{ \'SIGNUP_BUTTON\' | translate }}</button>\n							</div>\n						\n					</div>\n				</div>\n			</div>\n		</div>\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/login/login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_user__["a" /* User */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupPage = (function () {
    function SignupPage(navCtrl, user, toastCtrl, translateService, formBuilder, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.viewCtrl = viewCtrl;
        // The account fields for the login form.
        // If you're using the username field with or without email, make
        // sure to add it to the type
        this.account = {
            firstname: '',
            lastname: '',
            email: '',
            profession: '1',
            password: '',
            confirm_password: '',
            title: '1'
        };
        this.submitClient = false;
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        this.form = formBuilder.group({
            first_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            last_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].pattern(EMAIL_REGEXP)])],
            profession_id: [''],
            password: [''],
            confirm_password: [''],
            title_id: [''],
            country_id: ['83']
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    SignupPage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        this.viewCtrl.dismiss(this.form.value);
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-signup',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/signup/signup.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title text-center>{{ \'SIGNUP_TITLE\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="pb-0">\n  <form *ngIf="form" [formGroup]="form" (ngSubmit)="doSignup()">\n    <ion-list padding class="mb-0">\n\n      <ion-list radio-group formControlName="title_id">\n        <ion-row>\n        <ion-label stacked class="mb-2">Title</ion-label>\n      </ion-row>\n        <ion-row class="radio-btn">\n          <ion-col class="p-0">\n            <ion-item>\n              <ion-label>Dr</ion-label>\n              <ion-radio checked="true" value="1"></ion-radio>\n            </ion-item>\n          </ion-col>\n          <ion-col class="p-0">\n            <ion-item>\n              <ion-label>Mr</ion-label>\n              <ion-radio value="2"></ion-radio>\n            </ion-item>\n          </ion-col>\n          <ion-col class="p-0">\n            <ion-item>\n              <ion-label>Mrs</ion-label>\n              <ion-radio value="3"></ion-radio>\n            </ion-item>\n          </ion-col>\n          <ion-col class="p-0">\n            <ion-item>\n              <ion-label>Prof</ion-label>\n              <ion-radio value="4"></ion-radio>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n      \n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>First Name</ion-label>\n            <ion-input type="text" formControlName="first_name"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>Last Name</ion-label>\n            <ion-input type="text" formControlName="last_name"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label stacked>{{ \'EMAIL\' | translate }}</ion-label>\n          <ion-input type="email" formControlName="email"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col>\n        <ion-item>\n          <ion-label stacked>Profession</ion-label>\n          <ion-select formControlName="profession_id" name="profession">\n            <ion-option value="1">Psychologist</ion-option>\n            <ion-option value="2">Psychatrist</ion-option>\n            <ion-option value="3">Counsllor</ion-option>\n            <ion-option value="4">Medical Practitioner</ion-option>            \n            <ion-option value="5">Psychology Student</ion-option>\n            <ion-option value="6">Occupational Therapist</ion-option>\n            <ion-option value="7">Psychiatric Nurse</ion-option>\n            <ion-option value="8">Social Worker</ion-option>\n            <ion-option value="9">Student</ion-option>            \n            <ion-option value="10">Researcher</ion-option>\n            <ion-option value="11">Other</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n      <!--\n      Want to add a Username? Here you go:\n\n      <ion-item>\n        <ion-label floating>Username</ion-label>\n        <ion-input type="text" [(ngModel)]="account.username" name="username"></ion-input>\n      </ion-item>\n      -->\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>{{ \'PASSWORD\' | translate }}</ion-label>\n            <ion-input type="password" formControlName="password" name="password"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>Confirm Password</ion-label>\n            <ion-input type="password" formControlName="confirm_password" name="password"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <ion-item>\n            <ion-label stacked>View Terms And Conditions of Registration</ion-label>           \n          </ion-item>\n        </ion-col>      \n      </ion-row>\n\n  </ion-list>  \n  </form>\n</ion-content>\n<ion-footer border>\n  <ion-buttons right>\n    <button ion-button clear icon-end (click)="done()" [disabled]="!isReadyToSave">\n      Register\n  <ion-icon name="ios-arrow-dropright-circle"></ion-icon>\n</button>\n  </ion-buttons>\n</ion-footer>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/signup/signup.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_user__["a" /* User */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdministerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AdministerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AdministerPage = (function () {
    function AdministerPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.clientName = this.navParams.get('clientName');
    }
    AdministerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdministerPage');
        console.log('value of clientName passed' + this.clientName);
    };
    AdministerPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    AdministerPage.prototype.administer = function () {
        this.getQuestionsForAssessment();
    };
    AdministerPage.prototype.getQuestionsForAssessment = function () {
    };
    return AdministerPage;
}());
AdministerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-administer',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/administer/administer.html"*/'<!--\n  Generated template for the AdministerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content padding>\n\n    <div id="main" class="inner-page assessment-page fullpage">\n        <div class="container-fluid">\n          <div class="row">\n            <div class="fullpage-popup w-100">\n              <div class="popup-header">\n                <div class="row">\n                  <div class="col-md-10">\n                    <h3>Assessment 1 of 2</h3>\n                  </div>\n                  <div class="col-md-2 text-right">\n                    <button class="btn" (click)="cancel()">\n                      <img src="/assets/images/close.png">\n                    </button>\n                  </div>\n                </div>\n              </div>\n              <div class="popup-body text-center">\n                <img src="/assets/images/svg/novopsych-logo.svg" style="width:120px;" alt="">\n                <h3 class="mt-2 mb-0">{{clientName}}</h3>\n                <p class="lead mt-1">DASS-21</p>\n                <button class="mt-2 btn btn-success">Start Assessment</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/administer/administer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], AdministerPage);

//# sourceMappingURL=administer.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectAssessmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__administer_administer__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SelectAssessmentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * the tags module is from https://github.com/HsuanXyz/ionic-tags-input
 */
var SelectAssessmentPage = (function () {
    function SelectAssessmentPage(navCtrl, navParams, viewCtrl, items) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.items = items;
        this.clientId = this.navParams.get('clientId');
        this.clientName = this.navParams.get('clientName');
        this.assessmentsTag = [];
        this.assessmentNameTag = [];
        this.selected = [];
        this.selectedAssessments = {
            short_name: 'First Name',
            extended_name: 'Last Name'
        };
    }
    SelectAssessmentPage.prototype.ionViewDidEnter = function () {
        console.log('value of clientId passed' + this.clientId);
        this.assessments = this.items.getAssessments();
        console.log(this.assessmentsTag.length);
        console.log(this.assessmentNameTag.length);
        //this.assessment = this.assessments[0];
    };
    SelectAssessmentPage.prototype.cancel = function () {
        console.log(this.assessmentsTag.length);
        this.assessmentsTag.length = 0;
        console.log(this.assessmentsTag.length);
        this.viewCtrl.dismiss();
    };
    SelectAssessmentPage.prototype.assessmentSelected = function (item) {
        var index = this.assessmentsTag.indexOf(item);
        if (index > -1) {
            console.log("deleting item to assessmentsTag array");
            this.assessmentsTag.pop();
            this.assessmentNameTag.pop();
            item.selected = false;
        }
        else {
            console.log("adding item to assessmentsTag array");
            //little hack to display the tag name
            this.assessmentsTag.push(item);
            this.assessmentNameTag.push(item.short_name);
            item.selected = true;
        }
    };
    SelectAssessmentPage.prototype.onChange = function (val) {
        this.assessmentsTag.pop();
        this.assessmentNameTag.pop();
    };
    SelectAssessmentPage.prototype.filterAssessments = function (ev) {
        var val = ev.target.value;
        this.assessments = this.items.getAssessments(val);
        return this.assessments;
    };
    SelectAssessmentPage.prototype.administer = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__administer_administer__["a" /* AdministerPage */], { clientName: this.clientName });
    };
    return SelectAssessmentPage;
}());
SelectAssessmentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-select-assessment',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/select-assessment/select-assessment.html"*/'<!--\n  Generated template for the SelectAssessmentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>Select Assessment(s) for Estelle Potter</ion-title>\n\n    \n    <ion-buttons right>\n      <button ion-button icon-only (click)="cancel()" end>\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n  <ion-list inset> \n    <button ion-item *ngFor="let item of assessments " (click)="assessmentSelected(item)">\n            <h2>{{item.short_name}} {{item.extended_name}}</h2>\n          </button>\n  </ion-list>\n\n  \n  <ion-tags-input [(ngModel)]="assessmentNameTag" [placeholder]="\'add test\'" (onChange)="onChange($event)"></ion-tags-input>\n  \n</ion-content> -->\n\n<ion-content>\n  \n  <!-- Assessment(s) for Estelle Potter Modal -->\n  <div class="popup">\n      <div class="popup-dialog" role="document">\n        <div class="popup-content">\n          <div class="popup-header">\n            <div class="popup-title">\n              <div class="d-flex w-100">\n                <div class="col-md-1">\n                  \n                </div>\n                <div class="col-md-10 text-center">\n                  <h3>Assessment(s) for {{clientName}}</h3>\n                </div>\n                <div class="col-md-1">\n                  <button class="btn-icon" (click)="cancel()">\n                    <img src="./assets/images/close.png">\n                  </button>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class="popup-body p-0 pt-1">\n            \n            <div class="popup-search">\n                <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n            </div>\n            <ion-list class="list-group">\n                <button ion-item class="list-group-item" [class.checked]="item.selected" *ngFor="let item of assessments " (click)="assessmentSelected(item)">\n                    <strong>{{item.short_name}}</strong>\n                    <span>  - {{item.extended_name}}</span>\n                    <span class="float-right">\n                        <span class="check"></span>\n                    </span>\n                </button>\n            </ion-list>\n            <div class="bg-white p-1">\n                  <ion-tags-input [(ngModel)]="assessmentNameTag" [once]="\'true\'" [placeholder]="\'Selected tests\'" (onChange)="onChange($event)"></ion-tags-input>\n            </div>\n          </div>\n          <div class="popup-footer text-right">\n            <button class="btn-icon" (click)="administer(assessmentNameTag)">\n              <strong>ADMINISTER ({{assessmentNameTag.length}})</strong>\n              <img src="./assets/images/svg/start.svg" alt="">\n            </button>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- // Assessment(s) for Estelle Potter Modal -->\n  \n  </ion-content>\n\n\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/select-assessment/select-assessment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__providers_providers__["a" /* Items */]])
], SelectAssessmentPage);

//# sourceMappingURL=select-assessment.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SchedulePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SchedulePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SchedulePage = (function () {
    function SchedulePage(navCtrl, navParams, viewCtrl, items) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.items = items;
        this.schedule = "Select Assessment";
        this.clientId = this.navParams.get('clientId');
        this.assessmentsTag = [];
        this.assessmentNameTag = [];
        this.selectedAssessments = {
            short_name: 'First Name',
            extended_name: 'Last Name'
        };
        this.schedule_ = {
            date: "",
            frequency: "",
        };
    }
    SchedulePage.prototype.ionViewDidEnter = function () {
        console.log('value of clientId passed' + this.clientId);
        this.assessments = this.items.getAssessments();
    };
    SchedulePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    SchedulePage.prototype.assessmentSelected = function (item) {
        //little hack to display the tag name
        this.assessmentsTag.push(item);
        this.assessmentNameTag.push(item.short_name);
    };
    SchedulePage.prototype.onChange = function (val) {
        this.assessmentsTag.pop();
        this.assessmentNameTag.pop();
    };
    SchedulePage.prototype.filterAssessments = function (ev) {
        var val = ev.target.value;
        this.assessments = this.items.getAssessments(val);
        return this.assessments;
    };
    return SchedulePage;
}());
SchedulePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-schedule',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/schedule/schedule.html"*/'\n\n<ion-content>\n    <div [ngSwitch]="schedule">\n    <!-- Assessment(s) for Estelle Potter Modal -->\n    <div class="popup">\n        <div class="popup-dialog" role="document">\n          <div class="popup-content">\n            <div class="popup-header">\n              <div class="popup-title">\n                <div class="d-flex w-100">\n                  <div class="col-md-1">\n                  </div>\n                  <div class="col-md-10 text-center">\n                    <h3>Email to {{clientName}}</h3>\n                    <p class="small mb-0" *ngIf="clientEmail != \'\' ">{{clientEmail}}</p>\n                    <p class="small mb-0" *ngIf="clientEmail == \'\' ">No Email Provided</p>\n                  </div>\n                  <div class="col-md-1">\n                    <button class="btn-icon" (click)="cancel()">\n                      <img src="./assets/images/close.png">\n                    </button>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class="bg-white">\n            <ion-segment [(ngModel)]="schedule" name="schedule" color="light">\n                <ion-segment-button value="Select Assessment">\n                  Select Assessment(s)\n                </ion-segment-button>\n                <ion-segment-button value="Schedule Settings">\n                    Schedule Settings\n                  </ion-segment-button>\n                <ion-segment-button value="Customise Email">\n                  Customise Email\n                </ion-segment-button>\n            </ion-segment>\n          </div>\n            <div class="popup-body p-0 pt-2" *ngSwitchCase="\'Select Assessment\'">\n              \n              <div class="popup-search">\n                  <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n              </div>\n              <ion-list class="list-group">\n                  <button ion-item class="list-group-item" [class.checked]="item.selected" *ngFor="let item of assessments " (click)="assessmentSelected(item)">\n                      <strong>{{item.short_name}}</strong>\n                      <span>&nbsp;&nbsp;- {{item.extended_name}}</span>\n                      <span class="float-right">\n                          <span class="check"></span>\n                      </span>\n                  </button>\n              </ion-list>\n              <div class="bg-white p-1">\n                    <ion-tags-input [(ngModel)]="assessmentNameTag" [once]="\'true\'" [placeholder]="\'Selected tests\'" (onChange)="onChange($event)" name="assessment_name_tag"></ion-tags-input>  \n              </div>\n            </div>\n            \n            <div class="popup-body pt-2" *ngSwitchCase="\'Customise Email\'">\n                <div class="row">\n                  <div class="col">\n                    <div class="form-group">\n                      <label for="note" class="text-muted small">Subject</label>\n                      <ion-input name="note" id="note" class="form-control" [(ngModel)]="sendEmail.subject" name="sendemail_object"></ion-input>\n                    </div>\n                    <div class="form-group">\n                      <label for="note" class="text-muted small">Message</label>\n                      <ion-textarea name="note" id="note" cols="30" rows="10" class="form-control" [(ngModel)]="sendEmail.body" name="sendemail_body"></ion-textarea>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div class="schedule_setting">\n                <div class="popup-body pt-2" *ngSwitchCase="\'Schedule Settings\'">\n                  <div class="row">\n                    <div class="col">\n                      <form name="form1">\n                      <h4>Start On:</h4>\n                      <div class="form-group">\n                        <label for="" class="text-muted small">Select Date</label>\n                        <!-- <input type="text" class="form-control w-25"> -->\n                        <ion-datetime class = "form-control w-25" displayFormat="DD MMM YYYY" min="1960" max="2002-10-31" [(ngModel)]="schedule_.date" name="schedule_date"></ion-datetime>\n                      </div>\n\n                      <div class="form-group">\n\n                      <h4 class="mt-4">Repeat Every:</h4>\n                      <!-- <label for="" class="text-muted small">Select Frequency</label> -->\n                      <ion-list radio-group [(ngModel)]="schedule_.frequency" name="schedule_frequency">\n                        <ion-row class="mx-0">\n                          <ion-label stacked class="text-muted small">Select Frequency</ion-label>\n                        </ion-row>\n                          <ion-row class="radio-btn mx-0 mt-2">\n                            <ion-col class="p-0">\n                              <ion-item>\n                                <ion-label>None</ion-label>\n                                <ion-radio checked="true" value="0"></ion-radio>\n                              </ion-item>\n                            </ion-col>\n                            <ion-col class="p-0">\n                              <ion-item>\n                                <ion-label>Day</ion-label>\n                                <ion-radio value="1"></ion-radio>\n                              </ion-item>\n                            </ion-col>\n                            <ion-col class="p-0">\n                              <ion-item>\n                                <ion-label>Week</ion-label>\n                                <ion-radio value="2"></ion-radio>\n                              </ion-item>\n                            </ion-col>\n                            <ion-col class="p-0">\n                              <ion-item>\n                                <ion-label>Fortnight</ion-label>\n                                <ion-radio value="3"></ion-radio>\n                              </ion-item>\n                            </ion-col>\n                            <ion-col class="p-0">\n                                <ion-item>\n                                  <ion-label>Month</ion-label>\n                                  <ion-radio value="4"></ion-radio>\n                                </ion-item>\n                              </ion-col>\n                              <ion-col class="p-0">\n                                  <ion-item>\n                                    <ion-label>3 Months</ion-label>\n                                    <ion-radio value="5"></ion-radio>\n                                  </ion-item>\n                                </ion-col>\n                          </ion-row>\n                        </ion-list>\n                        \n                      </div>\n  \n                      <!-- <div class="radio-group">\n                        <div class="radio-input">\n                          <input type="radio" value="0" id="schedule0" [(ngModel)]="schedule_.frequency">\n                          <label for="radio0">None</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" value="1" id="schedule1" [(ngModel)]="schedule_.frequency">\n                          <label for="radio1">Day </label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" value="2" id="schedule2" [(ngModel)]="schedule_.frequency">\n                          <label for="radio2">Week</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" value="3" id="schedule3" [(ngModel)]="schedule_.frequency">\n                          <label for="radio3">Fortnight</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" value="4" id="schedule4" [(ngModel)]="schedule_.frequency">\n                          <label for="radio4">Month</label>\n                        </div>\n                        <div class="radio-input">\n                            <input type="radio" value="5" id="schedule5" [(ngModel)]="schedule_.frequency">\n                          <label for="radio5">3 Months</label>\n                        </div>\n                      </div> -->\n  \n                      <h4 class="mt-4">No of Repetitions:</h4>\n                      <label for="" class="text-muted small">Enter No(max 99)</label>\n                      <div>\n                        <ion-input type="number" class="form-control w-25 float-left" [(ngModel)]="schedule_.repetition" name="schedule_repetition"></ion-input>\n                        <span class="d-inline-block p-2">concludes on <strong>{{schedule_.date}}</strong></span>\n                      </div>\n                      </form>\n                    </div>\n                  </div>\n                </div>\n                </div>\n  \n            <div class="popup-footer text-right">\n              <div class="float-left">\n                <div class="attach_client">\n                  <div class="radio">\n                      <ion-checkbox [(ngModel)]="emailSelected" name="email_selected"></ion-checkbox>\n                    <label for="attach_client">Email result to clients</label>\n                  </div>\n                </div>\n              </div>\n              <button class="btn-icon" (click)="administer()">\n                <strong>ADMINISTER ({{assessmentNameTag.length}})</strong>\n                <img src="./assets/images/svg/start.svg" alt="">\n              </button>\n            </div>\n          </div>\n        </div>\n      </div>\n      <!-- // Assessment(s) for Estelle Potter Modal -->\n      </div>\n  </ion-content>'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/schedule/schedule.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Items */]])
], SchedulePage);

//# sourceMappingURL=schedule.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EmailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EmailPage = (function () {
    function EmailPage(navCtrl, navParams, viewCtrl, items) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.items = items;
        this.email = "Select Assessment";
        this.clientId = this.navParams.get('clientId');
        this.clientName = this.navParams.get('clientName');
        this.clientEmail = this.navParams.get('clientEmail');
        this.assessmentsTag = [];
        this.assessmentNameTag = [];
        this.selectedAssessments = {
            short_name: 'First Name',
            extended_name: 'Last Name'
        };
        this.sendEmail = {
            subject: '',
            body: ''
        };
    }
    EmailPage.prototype.ionViewDidEnter = function () {
        console.log('value of clientId passed' + this.clientId);
        this.assessments = this.items.getAssessments();
        //this.assessment = this.assessments[0];
    };
    EmailPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    EmailPage.prototype.assessmentSelected = function (item) {
        var index = this.assessmentsTag.indexOf(item);
        if (index > -1) {
            item.selected = false;
        }
        else {
            item.selected = true;
        }
        //little hack to display the tag name
        this.assessmentsTag.push(item.id);
        this.assessmentNameTag.push(item.short_name);
    };
    EmailPage.prototype.onChange = function (val) {
        this.assessmentsTag.pop();
        this.assessmentNameTag.pop();
    };
    EmailPage.prototype.filterAssessments = function (ev) {
        var val = ev.target.value;
        this.assessments = this.items.getAssessments(val);
        return this.assessments;
    };
    EmailPage.prototype.administer = function () {
        this.items.emailAssessments({
            client_id: this.clientId,
            assessment_id: this.assessmentsTag,
            email_subject: this.sendEmail.subject,
            email_body: this.sendEmail.body,
            client_email_results: this.emailSelected
        }).subscribe(function (resp) {
            console.log("email send successfully.");
        }, function (err) {
            console.log("email not send ");
        });
    };
    return EmailPage;
}());
EmailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-email',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/email/email.html"*/'\n\n<ion-content>\n  <div [ngSwitch]="email">\n  <!-- Assessment(s) for Estelle Potter Modal -->\n  <div class="popup">\n      <div class="popup-dialog" role="document">\n        <div class="popup-content">\n          <div class="popup-header">\n            <div class="popup-title">\n              <div class="d-flex w-100">\n                <div class="col-md-1">\n                </div>\n                <div class="col-md-10 text-center">\n                  <h3>Email to {{clientName}}</h3>\n                  <p class="small mb-0" *ngIf="clientEmail != \'\' ">{{clientEmail}}</p>\n                  <p class="small mb-0" *ngIf="clientEmail == \'\' ">No Email Provided</p>\n                </div>\n                <div class="col-md-1">\n                  <button class="btn-icon" (click)="cancel()">\n                    <img src="./assets/images/close.png">\n                  </button>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class="bg-white">\n          <ion-segment [(ngModel)]="email" color="light">\n              <ion-segment-button value="Select Assessment">\n                Select Assessment(s)\n              </ion-segment-button>\n              <ion-segment-button value="Customise Email">\n                Customise Email\n              </ion-segment-button>\n          </ion-segment>\n          </div>\n          <div class="popup-body p-0 pt-2" *ngSwitchCase="\'Select Assessment\'">\n            \n            <div class="popup-search">\n                <ion-searchbar (ionInput)="filterAssessments($event)"></ion-searchbar>\n            </div>\n            <ion-list class="list-group">\n                <button ion-item class="list-group-item" [class.checked]="item.selected" *ngFor="let item of assessments " (click)="assessmentSelected(item)">\n                    <strong>{{item.short_name}}</strong>\n                    <span> - {{item.extended_name}}</span>\n                    <span class="float-right">\n                        <span class="check"></span>\n                    </span>\n                </button>\n            </ion-list>\n            <div class="bg-white p-1">\n                  <ion-tags-input [(ngModel)]="assessmentNameTag" [once]="\'true\'" [placeholder]="\'Selected tests\'" (onChange)="onChange($event)"></ion-tags-input>  \n            </div>\n          </div>\n\n          <div class="popup-body pt-2" *ngSwitchCase="\'Customise Email\'">\n              <div class="row">\n                <div class="col">\n                  <div class="form-group">\n                    <label for="note" class="text-muted small">Subject</label>\n                    <ion-input name="note" id="note" class="form-control" [(ngModel)]="sendEmail.subject"></ion-input>\n                  </div>\n                  <div class="form-group">\n                    <label for="note" class="text-muted small">Message</label>\n                    <ion-textarea name="note" id="note" cols="30" rows="10" class="form-control" [(ngModel)]="sendEmail.body"></ion-textarea>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n      <!-- // Assessment(s) for Estelle Potter Modal -->\n      </div>\n    </ion-content>\n    <ion-footer>\n      <div class="popup">\n        <div class="popup-dialog">\n          <div class="popup-footer text-right">\n            <div class="float-left">\n              <div class="attach_client">\n                <div class="radio">\n                  <ion-checkbox [(ngModel)]="emailSelected"></ion-checkbox>\n                  <label for="attach_client">Email result to clients</label>\n                </div>\n              </div>\n            </div>\n            <button class="btn-icon" [disabled]="!isReadyToSend" (click)="administer()">\n              <strong>ADMINISTER ({{assessmentNameTag.length}})</strong>\n              <img src="./assets/images/svg/start.svg" alt="">\n            </button>\n          </div>\n          </div>\n        </div>\n        </ion-footer>  \n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/email/email.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Items */]])
], EmailPage);

//# sourceMappingURL=email.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_providers__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ReportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ReportPage = (function () {
    function ReportPage(sanitizer, navCtrl, navParams, modalCtrl, viewCtrl, items) {
        var _this = this;
        this.sanitizer = sanitizer;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.items = items;
        this.link = this.navParams.get('url');
        this.items.getReport(this.link).subscribe(function (data) {
            var file3 = new Blob([data._body], { type: 'application/pdf' });
            _this.reportUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(file3));
        }, function (error) {
            console.log(error);
        });
    }
    ReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportPage');
    };
    ReportPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return ReportPage;
}());
ReportPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-report',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/report/report.html"*/'<!--\n  Generated template for the ReportPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n  <!-- <ion-content class= \'padding has-subheader\'>\n    <iframe class= \'webPage\' name= "recentActivitiesPage" src=\'sanitizer.bypassSecurityTrustResourceUrl(options.url)\' width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>\n   </iframe>\n  </ion-content> -->\n\n  \n      <!-- <ion-icon large name="close-circle" (click)="closeModal()" float-right style="z-index:9999;font-size:3.5em;padding:20px"></ion-icon> -->\n      <!-- <pdf-viewer [src]="options" [render-text]="true" style="display: block;"></pdf-viewer> -->\n  \n    <div *ngIf="reportUrl != undefined">\n        <iframe width="500" height="600" [attr.src]="reportlUrl" type="application/pdf"></iframe>\n  </div>    \n\n<!--<ion-header>\n\n  <ion-navbar>\n    <ion-title>report</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    \n  <iframe src="http://www.w3schools.com"></iframe>\n       <div ng-include src="\'http://www.w3schools.com\'"></div>\n    \n</ion-content>-->\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/report/report.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__providers_providers__["a" /* Items */]])
], ReportPage);

//# sourceMappingURL=report.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddNotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddNotePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddNotePage = (function () {
    function AddNotePage(navCtrl, viewCtrl, formBuilder, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.note = {
            title: 'New Note',
            body: '',
            client_id: ''
        };
        this.submitClient = false;
        if (this.navParams.data.note) {
            console.log(this.navParams.data.note);
            this.note.body = this.navParams.data.note.body;
            this.note.title = this.navParams.data.note.title;
        }
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        this.form = formBuilder.group({
            title: [this.note.title],
            body: [this.note.body, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
        // Watch the form for changes, and
        this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
    }
    AddNotePage.prototype.ionViewDidLoad = function () {
        console.log('inside ionViewDidLoad AddNotePage');
    };
    AddNotePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    AddNotePage.prototype.done = function () {
        if (!this.form.valid) {
            return;
        }
        this.viewCtrl.dismiss(this.form.value);
    };
    return AddNotePage;
}());
AddNotePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-note',template:/*ion-inline-start:"/home/linus/Projects/novodebugrepo/src/pages/add-note/add-note.html"*/'<!--\n  Generated template for the AddNotePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- Add Note Modal -->\n\n<ion-content >\n    <form *ngIf="form" [formGroup]="form">\n<div class="popup m-0">\n    <div class="popup-dialog" role="document">\n      <div class="popup-content">\n        <div class="popup-header">\n          <div class="popup-title">\n            <div class="d-flex w-100">\n              <div class="col-md-1"></div>\n              <div class="col-md-10 text-center">\n                <h3>Add Note</h3>\n              </div>\n              <div class="col-md-1">\n                <button class="btn-icon" (click)="cancel()">\n                  <img src="./assets/images/close.png">\n                </button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class="popup-body">\n          <label for="note" class="text-muted small">Type Here</label>\n          <ion-textarea formControlName="body" id="note" cols="30" rows="12" class="form-control"></ion-textarea>\n          <div class="note-btns text-right mt-3">\n            <button class="btn-icon mr-3">\n              <img src="./assets/images/svg/trash.svg" class="minus-top-2" alt="">\n              <span>Delete Note</span>\n            </button>\n            <button class="btn btn-success" (click)="done()">Add Note</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n    </form>\n</ion-content>\n  <!-- // Add Note Modal -->\n'/*ion-inline-end:"/home/linus/Projects/novodebugrepo/src/pages/add-note/add-note.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], AddNotePage);

//# sourceMappingURL=add-note.js.map

/***/ })

},[261]);
//# sourceMappingURL=main.js.map